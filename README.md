<!--
SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdiand Rewicki

SPDX-License-Identifier: Apache-2.0
-->

Is it worth it? Comparing six deep and classical methods for unsupervised anomaly detection in time series
--------------
This repository contains the accompanying code to reproduce the results of the publication:
>Ferdinand Rewicki, Joachim Denzler, and Julia Niebling. 2023. "Is It Worth It? Comparing Six Deep and Classical Methods for Unsupervised Anomaly Detection in Time Series" Applied Sciences 13, no. 3: 1778. https://doi.org/10.3390/app13031778 

We compare three classical and three deep learning methods for anomaly detection in time series on the [UCR Anomaly Archive Dataset](#dataset).
The compared methods are:
* [Robust Random Cut Forest (RRCF)](#rrcf)
* [Maximally Divergent Intervals (MDI)](#mdi)
* [MERLIN](#merlin)
* Autoencoder
* [Graph Augmented Normalizing Flows (GANF)](#ganf)
* [Deep Transformer Networks for Anomaly Detection in Multivariate Time Series Data (TranAD)](#tranad)

Abstract
--------
Detecting anomalies in time series data is important in a variety of fields, including system monitoring, healthcare, and cybersecurity. While the abundance of available methods makes it difficult to choose the most appropriate method for a given application, each method has its strengths in detecting certain types of anomalies. In this study, we compare six unsupervised anomaly detection methods of varying complexity to determine whether more complex methods generally perform better and if certain methods are better suited to certain types of anomalies. We evaluated the methods using the UCR anomaly archive, a recent benchmark dataset for anomaly detection. We analyzed the results on a dataset and anomaly type level after adjusting the necessary hyperparameters for each method. Additionally, we assessed the ability of each method to incorporate prior knowledge about anomalies and examined the differences between point-wise and sequence-wise features. Our experiments show that classical machine learning methods generally outperform deep learning methods across a range of anomaly types.

Dependencies
------------
* Python >= 3.9

For building MDI
* c++ compiler compatible with c++ standard version 11, tested with g++ version 9.4.0
* [cmake](https://cmake.org/) >= 3.1.0, tested with cmake version 0.4.0
* [Eigen3](https://eigen.tuxfamily.org), tested with eigen3 version 3.3.7
* PkgConfig, tested with version 0.29.1
* [OpenMP](https://www.openmp.org/), tested with version 4.5

Installation
-----------
Clone repository:
```
git clone git@gitlab.com:dlr-dw/is-it-worth-it-benchmark.git
cd  is-it-worth-it-benchmark
```

To install the dependencies, please run
```
pip install -e . --upgrade
```

### Install `UCR Anomaly Archive`
To download the [UCR Anomaly Archive](https://www.cs.ucr.edu/%7Eeamonn/time_series_data_2018/) run
```bash
scripts/install_ucr_dataset.sh
```
or execute the following steps manually:
```bash
mkdir ./data
mkdir ./tmp 
wget -P ./tmp https://www.cs.ucr.edu/~eamonn/time_series_data_2018/UCR_TimeSeriesAnomalyDatasets2021.zip
unzip ./tmp/UCR_TimeSeriesAnomalyDatasets2021.zip -d "ucraa"
mv ./tmp/ucraa/AnomalyDatasets_2021/UCR_TimeSeriesAnomalyDatasets2021/FilesAreInHere/UCR_Anomaly_FullData ./data/ucraa
rm -rf ./tmp
```

### Install `libmaxdiv`
The library [libmaxdiv](https://cvjena.github.io/libmaxdiv/) providing the method `Maximally Divergent Intervals (MDI)`  can be installed by running
```bash
./scripts/install_mdi.sh
```
or by executing the following steps manually:
```
mkdir ./tmp
wget -P ./tmp https://github.com/cvjena/libmaxdiv/archive/refs/tags/v1.1.zip
unzip ./tmp/v1.1.zip -d ./tmp
mv ./tmp/libmaxdiv-1.1 ./lib/libmaxdiv
touch ./lib/libmaxdiv/__init__.py

cd ./lib/libmaxdiv/maxdiv/libmaxdiv && mkdir bin && cd bin
cmake ..
make

cd ../../../../../
rm -rf ./tmp
  ```

Experiments - Reproduction of results
-------------------------------------
To reproduce the results given in the paper please run the following steps:
1. Start mlflow server
2. Perform hyperparameter tuning.
   * The determined values for each method need then to be put in the configuration file in `conf/experiments/main/<method>.yaml`. 
   * This step can be omitted. In this case our values for the respective hyperparameters will be used.)
3. Run main experiments
   * Baseline runs (with seed)
   * Additional runs (without seed)
   * Additional runs for MDI and MERLIN with different subsequence lengths
4. Evaluation of results

## 1. Start mlflow server
All results are collected in mlflow and can be viewed and downloaded from there.
To start the mlflow server run:

```
mkdir -p out/mlflow && mlflow server --host 0.0.0.0 -p 5000 --backend-store-uri sqlite:///out/mlflow/mlflow.sqlite --default-artifact-root $(pwd)/out/mlflow/artifacts/ --gunicorn-opts "--timeout 180"
```

You can access th the mlflow UI at: http://127.0.0.1:5000


## 2. Hyperparameter Tuning
Hyperparameter tuning is implemented via hydra + optuna. Results can be viewed in MLFlow.

To perform the hyperparameters run:
```bash
 python utsadbench/experiments/hyperopt.py --multirun +experiment=hyperopt/<model> 
```
where `<model>` is one of `ae,ganf,mdi,merlin,rrcf,rrcf_sequences,tranad`

The configuration files for the hyperopt experiments can be found in `experiment/hyperopt`.

## 3. Main Experiments

### 3.1 Baseline runs (with seed)
This runs generate the main results
```bash
python utsadbench/experiments/main.py +experiment=main/<model>
```
where `<model>` is one of `ae,ganf,mdi,merlin,rrcf,rrcf_sequences,tranad`.

To run the main experiment in batches, use the available arguments. E.g. to run the autoencoder experiment in batches of 20 time series run the following command to start the third batch:
```bash
python utsadbench/experiments/main.py +experiment=ucr/ae dataset.dataset.start=40 dataset.dataset.n=20
```
The configuration files for the main experiments can be found in `experiment/main`.

### 3.2 Additional runs (without seed)
We performed 5 additional runs without seeding the random number generators.
```bash
for i in {2..6}; do
    python utsadbench/experiments/main.py +experiment=main/<model> seed=null tags.run_id=$i 
done
```
where `<model>` is one of `ae,ganf,mdi,merlin,rrcf,rrcf_sequences,tranad`.

### 3.3 Additional runs for MDI and MERLIN with different subsequence lengths
```bash
python utsadbench/experiments/main.py +experiment=main/mdi model.predict_params.l_min=100 model.predict_params.l_max=100 tags.run_id="fixed" mlflow.runname="MDI-subsequence-length"
python utsadbench/experiments/main.py +experiment=main/mdi model.predict_params.l_min=null model.predict_params.l_max=null tags.run_id="dynamic" mlflow.runname="MDI-subsequence-length"
python utsadbench/experiments/main.py +experiment=main/merlin model.predict_params.l_min=100 model.predict_params.l_max=100 tags.run_id="fixed" mlflow.runname="MERLIN-subsequence-length"
python utsadbench/experiments/main.py +experiment=main/merlin model.predict_params.l_min=null model.predict_params.l_max=null tags.run_id="dynamic" mlflow.runname="MERLIN-subsequence-length"
```

## 4. Evaluation of Results
To fetch the results from `mlflow` and recreate the plots provided in our paper please refer to the jupyter notebook `jupyter/evaluate_results.ipynb`by running
```bash
jupyter lab jupyter/evaluate_results.ipynb
```
Please take care that the mlflow tracking server is up and running.

Tests
-----
The tests can be executed by running:
```bash
pip install -e .[test] --upgrade
pytest .
```

Citation
--------
If you use this software, please cite using the metadata from the `CITATION.cff` file.

To cite the publication, please use:
```
@article{rewicki2023isit,
  author = {Rewicki, Ferdinand and Denzler, Joachim and Niebling, Julia},
  title = {Is It Worth It? Comparing Six Deep and Classical Methods for Unsupervised Anomaly Detection in Time Series},
  journal = {Applied Sciences},
  volume = {13},
  year = {2023},   
  number = {3},
  article-number = {1778},
  url = {https://www.mdpi.com/2076-3417/13/3/1778},
  doi = {10.3390/app13031778}
}
```

References
-------
### Dataset
> Wu, Renjie, and Eamonn Keogh. "Current time series anomaly detection  benchmarks are flawed and are creating the illusion of progress." IEEE Transactions on Knowledge and Data Engineering (2021).


### Libraries

The following packages in `utsadbench/lib` are re-distributions:

#### GANF
* Name: Graph-Augmented Normalizing Flows for Anomaly Detection of Multiple Time Series
* Repository: https://github.com/EnyanDai/GANF
* Citation:
  > Enyan Dai and Jie Chen. "Graph-Augmented Normalizing Flows for Anomaly Detection of Multiple Time Series.", International Conference on Learning Representations 2022, https://openreview.net/pdf?id=45L_dgP48Vd

#### POT
* Name: Peak over Threshold
* Repository: https://github.com/cbhua/model-pot
* Citation:
  > Alban Siffer, Pierre-Alain Fouque Alexandre Termier and Christine Largouet. "Anomaly detection in streams with extreme value theory." Proceedings of the 23rd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining. 2017.
  
#### TranAD
* Name: TranAD: Deep Transformer Networks for Anomaly Detection in Multivariate Time Series Data
* Repository: https://github.com/imperial-qore/TranAD
* Citation:
  > Shreshth Tuli, Casale Giuliano and Nicholas R. Jennings. "TranAD: Deep Transformer Networks for Anomaly Detection in Multivariate Time Series Data." Proceedings of VLDB, 2022 (pp. 1201-1214).

The following libraries are installed from source or via pip:

#### MDI
* Name: Maximally Divergent Intervals for Anomaly Detection
* Repository: https://github.com/cvjena/libmaxdiv
* Citation: 
  > Björn Barz, Erik Rodner, Yanira Guanche Garcia, Joachim Denzler. "Detecting Regions of Maximal Divergence for Spatio-Temporal Anomaly Detection." IEEE Transactions on Pattern Analysis and Machine Intelligence, 2018.

#### MERLIN
* Name: MERLIN: Parameter-Free Discovery of Arbitrary Length Anomalies in Massive Time Series Archives
* Repository: TBA
* Citation:
  >Takaaki Nakamura, Makoto Imamura, Ryan Mercer and Eamonn Keogh. "MERLIN: Parameter-Free Discovery of Arbitrary Length Anomalies in Massive Time Series Archives." IEEE International Conference on Data Mining, 2020.


#### RRCF
* Name: Robust Random Cut Forest
* Repository: https://github.com/kLabUM/rrcf
* Citation:
  > S. Guha, N. Mishra, G. Roy, & O. Schrijvers, Robust random cut forest based anomaly detection on streams, in Proceedings of the 33rd International conference on machine learning, New York, NY, 2016 (pp. 2712-2721).


License
-------
Please refer to the `LICENSE.md` file for further information about how the content is licensed.