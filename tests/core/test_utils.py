# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pytest

from utsadbench.core.utils import Collection, NamedCollection


class TestCollection:
    def test_it_can_be_created_from_list(self):
        items = [1, 2, 3]
        collection = Collection.create(items)
        assert isinstance(collection, Collection)

    def test_it_exposes_its_items(self):
        items = [1, 2, 3]
        collection = Collection.create(items)
        np.testing.assert_array_equal(items, collection.items)

    def test_it_can_be_iterated(self):
        items = [1, 2, 3]
        collection = Collection.create(items)

        for i, item in enumerate(collection):
            assert item == items[i]

    def test_it_can_be_created_empty(self):
        collection = Collection.empty()

        assert isinstance(collection, Collection)
        assert len(collection) == 0

    def test_it_can_be_created_iteratively(self):
        item1, item2, item3 = 1, 2, 3
        collection = Collection.empty()

        collection = collection.add(item1)
        collection = collection.add(item2)
        collection = collection.add(item3)

        assert len(collection) == 3
        np.testing.assert_array_equal(collection.items, [item1, item2, item3])

    def test_it_is_accessible_by_index(self):
        items = [1, 2, 3]
        collection = Collection.create(items)

        assert collection[0] == 1
        assert collection[1] == 2
        assert collection[2] == 3


class TestNamedCollection:
    def test_it_is_a_collection(self):
        items = [1, 2, 3]

        collection = NamedCollection.create(items, name="My Named Collection")

        assert isinstance(collection, Collection)

    def test_it_has_a_name(self):
        items = [1, 2, 3]
        name = "My Named Collection"

        collection = NamedCollection.create(items, name=name)
        collection2 = NamedCollection.empty(name=name)

        assert collection.name == name
        assert collection2.name == name

    def test_it_enforces_providing_a_name(self):
        with pytest.raises(ValueError, match="Missing argument 'name'!"):
            _ = NamedCollection.create([1, 2, 3])
        with pytest.raises(ValueError, match="Missing argument 'name'!"):
            _ = NamedCollection.empty()

        with pytest.raises(ValueError, match="'name' must not be empty string!"):
            _ = NamedCollection.create([1, 2, 3], name="")
        with pytest.raises(ValueError, match="'name' must not be empty string!"):
            _ = NamedCollection.empty(name="")
