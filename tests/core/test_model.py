# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import pytest
import torch.nn as nn
from pytest_mock import MockerFixture

from utsadbench.core.data import Sequence
from utsadbench.core.model import Model, ModelManager
from utsadbench.core.utils import Collection


class ModelImpl(Model, nn.Module):
    def do_train(self, **kwargs) -> "Model":
        pass

    def predict(
        self, sequences: Collection[Sequence], **kwargs
    ) -> Collection[Sequence]:
        pass

    def model_id(self):
        return "model-name"


class TestModel:
    def test_it_can_be_saved(self, mocker: MockerFixture):
        save = mocker.patch("utsadbench.core.model.ModelManager.save")
        model = ModelImpl()
        path = "path/to"
        name = "mymodel"

        model.save(path, name)

        save.assert_called_once_with(model, path, name)

    def test_it_can_be_loaded(self, mocker: MockerFixture):
        load = mocker.patch("utsadbench.core.model.ModelManager.load")
        path = "path/to/mymodel-name"
        device = mocker.patch("utsadbench.core.model.torch.device")
        device.type = "cpu"
        _ = ModelImpl.load(path, device)

        load.assert_called_once_with(path, device)

    def it_exposes_its_name(self):
        model = ModelImpl()
        assert model.name() == "model-name"


class TestModelManager:
    def test_save(self, mocker: MockerFixture):
        torch_save = mocker.patch("utsadbench.core.model.torch.save")
        path_mkdir = mocker.patch("utsadbench.core.model.pathlib.Path.mkdir")
        model = ModelImpl()

        ModelManager.save(model, "path/to", "mymodel")

        path_mkdir.assert_called_once_with(exist_ok=True, parents=True)
        torch_save.assert_called_once_with(model, "path/to/mymodel.pth")

    def test_load(self, mocker: MockerFixture):
        torch_load = mocker.patch("utsadbench.core.model.torch.load")
        exists = mocker.patch("utsadbench.core.model.pathlib.Path.exists")
        device = mocker.patch("utsadbench.core.model.torch.device")
        exists.return_value = True
        modelpath = "path/to/mymodel-name.pth"

        ModelManager.load(path=modelpath, device=device)

        torch_load.assert_called_once_with(modelpath, map_location=device)

    def test_it_verifies_that_the_path_exists(self, mocker: MockerFixture):
        with pytest.raises(
            RuntimeError, match="No model found for path path/to/mymodel-name.pth!"
        ):
            torch_load = mocker.patch("utsadbench.core.model.torch.load")
            exists = mocker.patch("utsadbench.core.model.pathlib.Path.exists")
            exists.return_value = False
            modelpath = "path/to/mymodel-name.pth"

            ModelManager.load(path=modelpath)

            torch_load.assert_not_called()
