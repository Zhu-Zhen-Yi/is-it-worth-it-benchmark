# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import pandas as pd

import utsadbench.core as core


class TestGapFilling:
    def test_it_fills_gaps_for_given_sampling_rate(self):
        # given
        df = pd.DataFrame.from_dict(
            {
                "timestamp": [
                    "2022-08-24 00:00:00",
                    "2022-08-24 00:00:30",
                    "2022-08-24 00:01:30",
                ],
                "values": [1, 2, 4],
            }
        )
        df.timestamp = pd.to_datetime(df.timestamp)

        # when
        df = core.io.fill_gaps(df, "30s", -1, field="values")

        # then
        assert df.iat[2, 1] == -1.0


class TestSubsampling:
    def test_it_subsamples_to_given_sampling_rate(self):
        # given
        df = pd.DataFrame.from_dict(
            {
                "timestamp": [
                    "2022-08-24 00:00:00",
                    "2022-08-24 00:00:30",
                    "2022-08-24 00:01:00",
                    "2022-08-24 00:01:30",
                    "2022-08-24 00:02:00",
                    "2022-08-24 00:02:30",
                ],
                "values": [1, 2, 3, 4, 5, 6],
            }
        )
        df.timestamp = pd.to_datetime(df.timestamp)

        # when
        df = core.io.subsample(df, "1min")

        # then
        assert len(df) == 3
        assert len(df["timestamp"]) == 3
        assert list(df["values"]) == [1.5, 3.5, 5.5]
