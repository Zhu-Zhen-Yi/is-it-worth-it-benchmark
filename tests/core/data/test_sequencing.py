# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pytest
import torch.utils.data as data

from utsadbench.core.data.dataset import TimeSeries
from utsadbench.core.data.sequencing import (
    Sequence,
    SequenceCollection,
    Sequencer,
    TorchDataLoader,
)


class TestSequence:
    def test_it_exposes_start_end_length(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        start, length = 1, 5

        sequence = Sequence(ts, start, length, 1)

        assert isinstance(sequence.ts, TimeSeries)
        assert sequence.start == start
        assert sequence.length == length
        assert sequence.end == 6

    def test_it_exposes_its_data(self):
        t1 = np.linspace(0, 9, 10)
        t2 = np.linspace(10, 19, 10)
        t3 = np.linspace(20, 29, 10)
        data = np.stack([t1, t2, t3]).transpose()
        expected_slice = np.array(
            [
                [1.0, 2.0, 3.0, 4.0, 5.0],
                [11.0, 12.0, 13.0, 14.0, 15.0],
                [21.0, 22.0, 23.0, 24.0, 25.0],
            ]
        ).transpose()
        ts = TimeSeries.from_numpy(data)

        sequence = Sequence(ts, 1, 5, 1)

        np.testing.assert_array_almost_equal(sequence.data, expected_slice)

    def test_it_can_have_custom_data(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        start, length = 1, 5
        data = np.array(
            [[0.0, 1.0, 2.0, 3.0, 4.0], [5.0, 6.0, 7.0, 8.0, 9.0]]
        ).transpose()

        sequence = Sequence(ts, start, length, 1, data=data)
        np.testing.assert_array_almost_equal(sequence.data, data)

    def test_it_has_anomaly_state(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))

        sequence_normal = Sequence(ts, 1, 5, 1)
        sequence_anomalous = Sequence(ts, 1, 5, 1, is_anomalous=True)

        assert not sequence_normal.is_anomalous
        assert sequence_anomalous.is_anomalous

    def test_it_expands_dimension_immutable(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))

        sequence = Sequence(ts, 1, 5, 1)
        expanded = sequence.expand_dims(axis=[0, 1])

        assert expanded.data.shape == (1, 1, 5, 2)
        assert expanded.data is not sequence.data

    def test_it_sqeueezes_dimensions_immutable(self):
        data = np.random.random((10, 1))
        ts = TimeSeries.from_numpy(data)

        sequence = Sequence(ts, 1, 5, 1)
        squeezed = sequence.squeeze()

        assert squeezed.data.shape == (5,)
        assert squeezed.data is not sequence.data


class TestSequenceCollection:
    def test_it_can_be_created_from_list(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        s1 = Sequence(ts, 1, 5, 1)
        s2 = Sequence(ts, 2, 5, 1)
        s3 = Sequence(ts, 3, 5, 1)

        sequences = SequenceCollection.create([s1, s2, s3])

        assert isinstance(sequences, SequenceCollection)

    def test_it_can_be_created_iteratively(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        s1 = Sequence(ts, 1, 5, 1)
        s2 = Sequence(ts, 2, 5, 1)
        s3 = Sequence(ts, 3, 5, 1)
        sequences = SequenceCollection.empty()

        sequences.add(s1)
        sequences.add(s2)
        sequences.add(s3)

        assert isinstance(sequences, SequenceCollection)
        assert len(sequences) == 3

    def test_it_checks_types_on_creation(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        s1 = Sequence(ts, 1, 5, 1)
        with pytest.raises(
            ValueError, match="'items' must be Sequence objects in SequenceCollection"
        ):
            _ = SequenceCollection.create([s1, "5"])

    def test_it_verifies_types_on_iterative_creation(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        s1 = Sequence(ts, 1, 5, 1)
        s2 = Sequence(ts, 2, 5, 1)
        with pytest.raises(ValueError, match="'item' must be of type Sequence"):
            sequences = SequenceCollection.empty()

            sequences.add(s1)
            sequences.add(s2)
            sequences.add("3")

    def test_it_can_be_split(self):
        ts = TimeSeries.from_numpy(np.random.random((104, 2)))
        sequences = Sequencer(5, 1).sequences(ts)

        train, test = sequences.split(test_ratio=0.2)

        assert isinstance(train, SequenceCollection)
        assert isinstance(test, SequenceCollection)
        assert len(train) == 80
        assert len(test) == 20


class TestSequencer:
    def test_it_is_initialized_with_length_and_stride(self):
        sequence_length = 5
        stride = 1

        sequencer = Sequencer(sequence_length, stride)

        assert isinstance(sequencer, Sequencer)

    def test_it_exposes_length_and_stride(self):
        sequence_length = 5
        stride = 1

        sequencer = Sequencer(sequence_length, stride)

        assert sequencer.seq_len == sequence_length
        assert sequencer.stride == stride

    def test_sequence_stride_1_no_padding(self):
        data = np.stack([np.linspace(0, 9, 10), np.linspace(10, 19, 10)]).transpose()
        ts = TimeSeries.from_numpy(data)
        sequencer = Sequencer(sequence_length=5, stride=1)

        sequences = sequencer.sequences(ts)

        assert len(sequences) == 6
        for i, sequence in enumerate(sequences):
            assert sequence.start == i
            np.testing.assert_array_equal(sequence.data, ts.data[i : i + 5, :])

    def test_sequence_stride_1_padding(self):
        data = np.stack([[1.0, 2.0, 3.0, 4.0], [3.0, 4.0, 5.0, 6.0]]).transpose()
        ts = TimeSeries.from_numpy(data)
        sequencer = Sequencer(sequence_length=3, stride=1, padding=True)

        sequences = sequencer.sequences(ts)

        expected = np.array(
            [
                [[1.0, 3.0], [1.0, 3.0], [1.0, 3.0]],
                [[1.0, 3.0], [1.0, 3.0], [1.0, 3.0]],
                [[1.0, 3.0], [1.0, 3.0], [2.0, 4.0]],
                [[1.0, 3.0], [2.0, 4.0], [3.0, 5.0]],
            ]
        )

        assert len(sequences) == 4
        for i, sequence in enumerate(sequences):
            assert sequence.start == i - 3
            np.testing.assert_array_equal(sequence.data, expected[i])

    def test_sequence_stride_k_no_padding_no_remainder(self):
        data = np.stack([np.linspace(0, 10, 11), np.linspace(10, 20, 11)]).transpose()
        ts = TimeSeries.from_numpy(data)
        sequencer = Sequencer(sequence_length=5, stride=2)

        sequences = sequencer.sequences(ts)

        assert len(sequences) == 4
        for i, sequence in enumerate(sequences):
            np.testing.assert_array_equal(sequence.data, ts.data[i * 2 : i * 2 + 5, :])

    @pytest.mark.filterwarnings("ignore::UserWarning")
    def test_sequence_stride_k_padding(self):
        data = np.stack([[1.0, 2.0, 3.0, 4.0], [3.0, 4.0, 5.0, 6.0]]).transpose()
        ts = TimeSeries.from_numpy(data)
        sequencer = Sequencer(sequence_length=3, stride=2, padding=True)

        sequences = sequencer.sequences(ts)

        expected = np.array(
            [
                [[1.0, 3.0], [1.0, 3.0], [1.0, 3.0]],
                [[1.0, 3.0], [1.0, 3.0], [2.0, 4.0]],
            ]
        )

        assert len(sequences) == 2
        for i, sequence in enumerate(sequences):
            np.testing.assert_array_equal(sequence.data, expected[i])

    def test_it_assigns_the_correct_anomaly_state(self):
        anomalies = [(3, 3), (13, 1)]
        ts = TimeSeries.from_numpy(np.random.random((20, 2)), anomalies=anomalies)
        expected_labels = [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0]

        sequences = Sequencer(3, 1).sequences(ts)

        labels = [s.is_anomalous for s in sequences]
        np.testing.assert_array_equal(labels, np.array(expected_labels).astype(bool))

    def test_it_raises_a_warning_when_using_padding_with_stride_k(self):
        with pytest.warns(
            UserWarning, match="Using Sequencer with padding and stride > 1."
        ):
            data = np.stack([[1.0, 2.0, 3.0, 4.0], [3.0, 4.0, 5.0, 6.0]]).transpose()
            ts = TimeSeries.from_numpy(data)
            sequencer = Sequencer(sequence_length=3, stride=2, padding=True)

            _ = sequencer.sequences(ts)


class TestTorchDataLoaderTest:
    def test_it_creates_an_unlabelled_dataloader_from_sequences(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)))
        sequences = Sequencer(5, 1).sequences(ts)

        dataloader = TorchDataLoader.unlabelled(sequences, batch_size=3, shuffle=False)

        assert isinstance(dataloader, data.DataLoader)
        for batch in dataloader:
            assert batch.numpy().shape == (3, 5, 2)

    def test_it_creates_a_labelled_dataloader_from_sequences(self):
        anomalies = [(3, 3), (13, 1)]
        ts = TimeSeries.from_numpy(np.random.random((20, 2)), anomalies=anomalies)
        sequences = Sequencer(3, 1).sequences(ts)
        expected_labels = [0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0]

        dataloader = TorchDataLoader.labelled(sequences, batch_size=1)

        assert isinstance(dataloader, data.DataLoader)
        labels = np.array([label.numpy() for _, label in dataloader])
        labels = labels.flatten()
        assert (labels == np.array(expected_labels).astype(bool)).all()
