# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pytest

from utsadbench.core.data.dataset import Anomaly, Dataset, TimeSeries
from utsadbench.core.utils import Collection


class TestAnomaly:
    def test_construction(self):
        start = 4
        length = 6
        anomaly = Anomaly(start, length)
        assert anomaly.start == 4
        assert anomaly.length == 6
        assert anomaly.end == 9


class TestTimeSeries:
    def test_it_can_be_created_from_numpy_array(self):
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data)
        assert isinstance(ts, TimeSeries)

    def test_it_can_be_created_from_list(self):
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data)
        assert isinstance(ts, TimeSeries)

    def test_it_can_be_named(self):
        name = "Time Series 1"
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data, name=name)
        assert ts.name == name

        ts = TimeSeries.from_numpy(data)
        assert ts.name == ""

    def test_it_has_a_length(self):
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data)
        assert ts.length == 5
        assert len(ts) == 5

    def test_it_has_a_dimension(self):
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data)
        assert ts.dim == 2

    def test_it_handles_univariate_data(self):
        data = np.random.random((5, 1))
        ts = TimeSeries.from_numpy(data)
        assert ts.dim == 1

    def test_it_handles_multivariate_data(self):
        data = np.random.random((5, 2))
        ts = TimeSeries.from_numpy(data)
        assert ts.dim == 2
        assert ts.length == 5

    def test_it_exposes_data(self):
        data = np.random.random((5, 1))
        ts = TimeSeries.from_numpy(data)
        assert ts.data.shape[0] == len(data)
        assert all([a == b for a, b in zip(ts.data[0], data)])

    def test_it_exposes_anomalies(self):
        # given
        data = np.random.random((20, 2))
        anomalies = [(3, 2), (9, 5)]

        # when
        ts = TimeSeries.from_numpy(data, anomalies)

        # then
        assert all([isinstance(a, Anomaly) for a in ts.anomalies])
        assert all(
            [
                a.start == e[0] and a.length == e[1]
                for a, e, in zip(ts.anomalies, anomalies)
            ]
        )

    def test_it_exposes_number_of_anomalies(self):
        data = np.random.random((20, 2))
        anomalies = [(3, 2), (9, 5)]

        ts1 = TimeSeries.from_numpy(data, anomalies)
        assert ts1.n_anomalies == 2

        ts2 = TimeSeries.from_numpy(data)
        assert ts2.n_anomalies == 0

    def test_it_exposes_labels(self):
        data = np.random.random((20, 2))
        anomalies = [(3, 2), (9, 5)]
        expected_labels = np.zeros(20)
        expected_labels[3:5] = 1
        expected_labels[9:14] = 1

        ts1 = TimeSeries.from_numpy(data, anomalies)
        assert (ts1.labels == expected_labels).all()

        ts2 = TimeSeries.from_numpy(data)
        assert (ts2.labels == np.zeros(20)).all()

    def test_it_can_be_anomalous(self):
        data = np.random.random((20, 2))
        anomalies = [(3, 2), (9, 5)]

        ts = TimeSeries.from_numpy(data, anomalies)

        assert ts.is_anomalous()

    def test_it_can_be_non_anomalous(self):
        data = np.random.random((20, 2))
        anomalies = []

        ts = TimeSeries.from_numpy(data, anomalies)

        assert not ts.is_anomalous()

    def test_it_exposes_min_max(self):
        data = np.array([[1, 2, 3, 4, 5], [6, 7, 8, 9, 10]]).transpose()
        ts = TimeSeries.from_numpy(data)

        assert ts.min == 1.0
        assert ts.max == 10.0

    def test_it_verifies_the_data_dtype(self):
        with pytest.raises(ValueError, match="'data' must have floating dtype!"):
            data = ["foo", "bar"]
            _ = TimeSeries.from_numpy(data)

        with pytest.raises(ValueError, match="'data' must be array-like!"):
            data = {"foo": 1}
            _ = TimeSeries.from_numpy(data)

    def test_it_verifies_the_data_shape(self):
        with pytest.raises(
            ValueError, match="'data' must not have more than 2 dimensions!"
        ):
            data = np.zeros((10, 2, 3))
            _ = TimeSeries.from_numpy(data)

    def test_it_verifies_anomalies(self):
        with pytest.raises(
            ValueError, match="'anomalies' must be given as list of tuples."
        ):
            data = np.random.random((20, 2))
            anomalies = [1, 3]

            _ = TimeSeries.from_numpy(data, anomalies)


class TestDataset:
    def test_dataset_is_a_named_collection(self):
        ts1 = TimeSeries.from_numpy(np.random.random((10, 2)))
        ts2 = TimeSeries.from_numpy(np.random.random((10, 2)))

        dataset = Dataset.create([ts1, ts2])

        assert isinstance(dataset, Collection)
        assert isinstance(dataset, Dataset)

    def test_type_checking_on_batch_creation(self):
        with pytest.raises(ValueError, match="'items' must be a list of TimeSeries"):
            _ = Dataset.create([1, 2])

    def test_it_can_be_created_iteratively(self):
        dataset = Dataset.empty()
        ts1 = TimeSeries.from_numpy(np.random.random((10, 2)))
        ts2 = TimeSeries.from_numpy(np.random.random((10, 2)))

        dataset = dataset.add(ts1)
        dataset = dataset.add(ts2)

        assert len(dataset) == 2

    def test_type_checking_on_iterative_creation(self):
        with pytest.raises(ValueError, match="'item' must have type TimeSeries!"):
            dataset = Dataset.empty()
            dataset = dataset.add(1)
