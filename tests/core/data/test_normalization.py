# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pytest

from utsadbench.core.data.dataset import Anomaly, Dataset, TimeSeries
from utsadbench.core.data.normalization import Denormalizer, Normalizer


class TestNormalizer:
    def test_it_normalizes_a_time_series(self):
        data = np.stack([np.linspace(1, 5, 5), np.linspace(11, 15, 5)]).transpose()
        anomalies = [(2, 3)]
        name = "Test Time Series"
        ts = TimeSeries.from_numpy(data, anomalies=anomalies, name=name)

        normalized = Normalizer.normalize(ts)

        assert isinstance(normalized, TimeSeries)
        assert normalized.min == 0.0
        assert normalized.max == 1.0
        assert isinstance(normalized.anomalies[0], Anomaly)
        assert normalized.anomalies[0].start == 2
        assert normalized.anomalies[0].length == 3
        assert normalized.name == name

    def test_it_is_idempotent(self):
        data = np.stack([np.linspace(0, 0.4, 5), np.linspace(0.6, 1, 5)]).transpose()
        # data = [[0.0, 0.1, 0.2, 0.3, 0.4], [0.6, 0.7, 0.8, 0.9, 1.0]]
        ts = TimeSeries.from_numpy(data)
        normalized1 = Normalizer.normalize(ts)

        normalized2 = Normalizer.normalize(ts)

        np.testing.assert_array_almost_equal(normalized2.data, normalized1.data)
        np.testing.assert_array_equal(normalized2.anomalies, normalized1.anomalies)

    def test_it_normalizes_a_dataset(self):
        ts1 = TimeSeries.from_numpy(np.random.random((10, 2)))
        ts2 = TimeSeries.from_numpy(np.random.random((10, 2)))
        dataset = Dataset.create([ts1, ts2])

        normalized = Normalizer.normalize(dataset)

        assert len(normalized) == 2
        assert all([isinstance(ts, TimeSeries) for ts in normalized])
        assert all([ts.min == pytest.approx(0.0) for ts in normalized])
        assert all([ts.max == pytest.approx(1.0) for ts in normalized])


class TestDenormalizer:
    def test_it_denormalizes_a_normalized_time_series(self):
        # given
        data = np.stack([np.linspace(1, 5, 5), np.linspace(11, 15, 5)]).transpose()
        anomalies = [(2, 3)]
        name = "Test Time Series"
        ts_origin = TimeSeries.from_numpy(data, anomalies, name)
        ts_norm = Normalizer.normalize(ts_origin)

        # when
        ts_denorm = Denormalizer.denormalize(ts_norm, ts_origin)

        # then
        assert isinstance(ts_denorm, TimeSeries)
        assert ts_denorm.n_anomalies == 1
        assert isinstance(ts_denorm.anomalies[0], Anomaly)
        np.testing.assert_array_almost_equal(ts_denorm.data, ts_origin.data)
        np.testing.assert_array_equal(ts_denorm.anomalies, ts_origin.anomalies)
        assert ts_denorm.name == name

    def test_it_denormalizes_a_normalized_dataset(self):
        ts1 = TimeSeries.from_numpy(np.random.random((10, 2)))
        ts2 = TimeSeries.from_numpy(np.random.random((10, 2)))
        ds_origin = Dataset.create([ts1, ts2])
        ds_norm = Normalizer.normalize(ds_origin)

        ds_denorm = Denormalizer.denormalize(ds_norm, ds_origin)
        assert len(ds_denorm) == 2
        assert all([isinstance(ts, TimeSeries) for ts in ds_denorm])
        for d, o in zip(ds_denorm, ds_origin):
            np.testing.assert_array_almost_equal(d.data, o.data)
            np.testing.assert_array_equal(d.anomalies, o.anomalies)

    def test_it_validates_arguments(self):
        with pytest.raises(
            ValueError, match="'normalize' and 'origin' must have same type!"
        ):
            ts1 = TimeSeries.from_numpy(np.random.random((10, 2)))
            ts2 = TimeSeries.from_numpy(np.random.random((10, 2)))
            dataset = Dataset.create([ts1, ts2])

            _ = Denormalizer.denormalize(ts1, dataset)
