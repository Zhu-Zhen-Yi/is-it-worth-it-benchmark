# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pytest

from utsadbench.core.data.dataset import TimeSeries
from utsadbench.core.data.normalization import Normalizer
from utsadbench.core.data.pipeline import DataPipeline
from utsadbench.core.data.sequencing import SequenceCollection, Sequencer


class TestDataPipeline:
    def test_it_requires_a_time_series_and_sequencer(self):
        ts = TimeSeries.from_numpy(np.random.random((5, 2)))
        sequencer = Sequencer(sequence_length=5, stride=1)

        pipeline = DataPipeline(ts, sequencer)

        assert isinstance(pipeline.data, TimeSeries)
        assert isinstance(pipeline.sequencer, Sequencer)
        assert pipeline.data == ts

    def test_it_accepts_normalizer(self):
        ts = TimeSeries.from_numpy(np.random.random((5, 2)))
        sequencer = Sequencer(sequence_length=5, stride=1)
        pipeline = DataPipeline(ts, sequencer, normalizer=Normalizer())

        assert isinstance(pipeline.normalizer, Normalizer)

    def test_it_can_be_run_with_dataset_only(self):
        ts = TimeSeries.from_numpy(np.random.random((5, 2)))
        sequencer = Sequencer(sequence_length=5, stride=1)
        pipeline = DataPipeline(ts, sequencer)

        result = pipeline.run()

        assert isinstance(result, SequenceCollection)

    def test_it_can_be_run_with_normalization(self):
        ts = TimeSeries.from_numpy(np.random.random((10, 2)) * 10)
        sequencer = Sequencer(sequence_length=5, stride=1)
        pipeline = DataPipeline(ts, sequencer, normalizer=Normalizer())

        result = pipeline.run()

        for sequence in result:
            assert pytest.approx(sequence.data.min(), 0)
            assert pytest.approx(sequence.data.max(), 1)
