# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import pathlib

import pytest

from utsadbench.core.data.dataset import Anomaly, TimeSeries
from utsadbench.data.ucr import UcrDataset, UcrTimeSeriesReader


class TestUcrDataset:
    def test_it_can_be_created_from_dir(self):
        datadir = "./tests/fixtures/ucr"

        ucr = UcrDataset.from_path(datadir)

        assert isinstance(ucr, UcrDataset)
        assert len(ucr.files) == 5

    def test_it_can_be_created_from_dir_sliced(self):
        datadir = "./tests/fixtures/ucr"

        expected_files = [
            "002_UCR_Anomaly_Series2_1500_2764_2995.txt",
            "003_UCR_Anomaly_Series3_2700_5920_5979.txt",
        ]

        ucr = UcrDataset.from_path(datadir, 1, 2)

        assert isinstance(ucr, UcrDataset)
        assert len(ucr.files) == 2
        assert ucr.files[0].name == expected_files[0]
        assert ucr.files[1].name == expected_files[1]

    def test_it_lazy_loads_timeseries_on_iteration(self):
        datadir = "./tests/fixtures/ucr"
        ucr = UcrDataset.from_path(datadir)

        for i, ts in enumerate(ucr):
            assert isinstance(ts, TimeSeries)
            assert ts.name == f"00{i+1}_UCR_Series{i+1}"

    def test_it_lazy_loads_timeseries_on_index_access(self):
        datadir = "./tests/fixtures/ucr"
        ucr = UcrDataset.from_path(datadir)

        ts = ucr[2]
        assert isinstance(ts, TimeSeries)
        assert ts.name == "003_UCR_Series3"

    def test_it_verifies_that_dir_exists(self):
        invalid_dir = "tests/fixtures/not_existing"

        with pytest.raises(
            ValueError, match=f"Directory {invalid_dir} does not exist."
        ):
            _ = UcrDataset.from_path(invalid_dir)


class TestUcrTimeSeriesReader:
    def test_it_loads_a_time_series_from_file(self):
        expected_length = 7321
        expected_anomaly = Anomaly(4675, 359)
        file = "001_UCR_Anomaly_Series1_1000_4675_5033.txt"
        path = "./tests/fixtures/ucr/" + file

        ts = UcrTimeSeriesReader.from_file(pathlib.Path(path))

        assert isinstance(ts, TimeSeries)
        assert len(ts) == expected_length
        assert ts.name == "001_UCR_Series1"
        assert ts.anomalies[0].start == expected_anomaly.start
        assert ts.anomalies[0].length == expected_anomaly.length
        assert ts.anomalies[0].end == expected_anomaly.end

    def test_it_verifies_that_the_file_exists(self):
        file = "./tests/fixtures/ucr/000_not_existing_file_0_0_0.txt"
        path = pathlib.Path(file)

        with pytest.raises(
            ValueError, match=f"File {path.name} not found in {path.parent}"
        ):
            _ = UcrTimeSeriesReader.from_file(pathlib.Path(path))

    def test_it_verifies_that_the_file_contains_data(self):
        file = "./tests/fixtures/ucr_invalid/000_one_empty_file_0_0_0.txt"
        path = pathlib.Path(file)

        with pytest.raises(RuntimeError, match=f"File {path.name} not readable!"):
            _ = UcrTimeSeriesReader.from_file(pathlib.Path(path))
