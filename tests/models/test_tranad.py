# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from functools import partial

import numpy as np
import torch

from utsadbench.core.data import Normalizer, Sequencer, TimeSeries
from utsadbench.models.tranad import TranAD


class TestTranAD:
    def test_it_returns_prediction_on_univariate_timeseries(self):
        ts = TimeSeries.from_numpy(np.random.random((100, 1)))
        ts_norm = Normalizer.normalize(ts)
        sequencer = Sequencer(sequence_length=10, stride=1, padding=True)
        sequences = sequencer.sequences(ts_norm)

        optimizer_partial = partial(torch.optim.Adam, lr=0.006, weight_decay=0.5)
        scheduler = partial(torch.optim.lr_scheduler.StepLR, gamma=0.9, step_size=5)
        model = TranAD(optimizer_partial, scheduler, n_feats=1)
        model.do_train(sequences, seed=42, epochs=1)

        scores, prediction = model.predict(sequences)

        assert prediction.length == 100
        assert prediction.dim == 1

    def test_it_returns_prediction_on_multivariate_timeseries(self):
        ts = TimeSeries.from_numpy(np.random.random((100, 2)))
        ts_norm = Normalizer.normalize(ts)
        sequencer = Sequencer(sequence_length=10, stride=1, padding=True)
        sequences = sequencer.sequences(ts_norm)

        optimizer_partial = partial(torch.optim.Adam, lr=0.006, weight_decay=0.5)
        scheduler = partial(torch.optim.lr_scheduler.StepLR, gamma=0.9, step_size=5)
        model = TranAD(optimizer_partial, scheduler, n_feats=2)
        model.do_train(sequences, seed=42, epochs=1)

        scores, prediction = model.predict(sequences)

        assert prediction.length == 100
        assert prediction.dim == 2
