Copyright (c) 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki

This work is licensed under multiple licenses:
* The source code and the accompanying material are licensed under Apache-2.0.
* The anomaly type annotations are licensed under CC0-1.0. 
* Insignificant files are licensed under CC0-1.0.
* The TranAD library in `lib/tranad` us licensed under the BSD-3 license. `Copyright (c) 2022, Shreshth Tuli`
* The POT library in `lib/ganf` licensed under the MIT license. `Copyright (c) 2021, Chuanbo Hua`

Please see the individual files for more accurate information.

> Hint: We provided the copyright and license information in accordance to the REUSE Specification 3.0.
