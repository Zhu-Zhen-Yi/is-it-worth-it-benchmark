# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import random

import mlflow
import numpy as np
import torch
from omegaconf import DictConfig, ListConfig


class Seeder:
    @staticmethod
    def seed(value: int):
        random.seed(value)
        np.random.seed(value)
        torch.random.manual_seed(value)
        torch.cuda.manual_seed(value)
        torch.cuda.manual_seed_all(value)


class MLFlowHelper:
    @staticmethod
    def init_experiment_from_config(cfg: DictConfig):
        # MLFlowHelper._init_experiment_with_sqlite_backend(cfg)
        MLFlowHelper._init_experiment_with_tracking_server(cfg)
        # MLFlowHelper._init_experiment_with_filestore_backend(cfg)

    @staticmethod
    def _init_experiment_with_filestore_backend(cfg: DictConfig):
        mlflow.set_tracking_uri(f"{cfg.mlflow.dir}/mlruns")
        mlflow.set_experiment(cfg.mlflow.runname)

    @staticmethod
    def _init_experiment_with_tracking_server(cfg: DictConfig):
        mlflow.set_tracking_uri(f"http://{cfg.mlflow.host}:{cfg.mlflow.port}")
        mlflow.set_experiment(cfg.mlflow.runname)

    @staticmethod
    def _init_experiment_with_sqlite_backend(cfg):
        mlflow.set_tracking_uri(f"sqlite:///{cfg.mlflow.dir}/mlflow.sqlite")

        experiment = mlflow.get_experiment_by_name(cfg.mlflow.runname)
        if experiment is None:
            id = mlflow.create_experiment(
                cfg.mlflow.runname, artifact_location=f"{cfg.mlflow.dir}/artifacts"
            )
            experiment = mlflow.get_experiment(id)
        mlflow.set_experiment(experiment.name)

    @staticmethod
    def log_params_from_omegaconf_dict(params):
        for param_name, element in params.items():
            if param_name == "tags":
                for tname, tval in element.items():
                    mlflow.set_tag(tname, tval)
            else:
                MLFlowHelper._explore_recursive(param_name, element)

    @staticmethod
    def _explore_recursive(parent_name, element):
        if isinstance(element, DictConfig):
            for k, v in element.items():
                if isinstance(v, DictConfig) or isinstance(v, ListConfig):
                    MLFlowHelper._explore_recursive(f"{parent_name}.{k}", v)
                else:
                    mlflow.log_param(f"{parent_name}.{k}", v)
        elif isinstance(element, ListConfig):
            for i, v in enumerate(element):
                mlflow.log_param(f"{parent_name}.{i}", v)
