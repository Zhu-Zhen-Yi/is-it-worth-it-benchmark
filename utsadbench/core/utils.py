# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

__all__ = ["Collection", "InflatableEnum", "NamedCollection"]

import enum
from typing import List, Sequence, TypeVar

T = TypeVar("T")


class Collection(Sequence[T]):
    def __init__(self, items: List[T]) -> None:
        self.items: List[T] = items

    @classmethod
    def create(cls, items: List[T], **kwargs) -> "Collection[T]":
        return cls(items)

    @classmethod
    def empty(cls, **kwargs) -> "Collection[T]":
        return cls([])

    def add(self, item: T) -> "Collection[T]":
        self.items.append(item)
        return self

    def __len__(self) -> int:
        return len(self.items)

    def __getitem__(self, item):
        return self.items[item]


class InflatableEnum(enum.Enum):
    def __str__(self):
        return self.value

    @classmethod
    def aslist(cls):
        return list(map(lambda v: v.value, cls))


class NamedCollection(Collection[T]):
    def __init__(self, items: List[T], name: str) -> None:
        super(NamedCollection, self).__init__(items)
        self.name = name

    @classmethod
    def create(cls, items: List[T], **kwargs) -> "NamedCollection[T]":
        name = cls._verify_name_param(**kwargs)

        return cls(items, name=name)

    @classmethod
    def empty(cls, **kwargs) -> "NamedCollection[T]":
        name = cls._verify_name_param(**kwargs)

        return cls([], name=name)

    @staticmethod
    def _verify_name_param(**kwargs) -> str:
        if "name" not in kwargs:
            raise ValueError("Missing argument 'name'!")
        if kwargs["name"] == "":
            raise ValueError("'name' must not be empty string!")

        return kwargs["name"]
