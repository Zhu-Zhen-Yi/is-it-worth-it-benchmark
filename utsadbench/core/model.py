# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

__all__ = ["Model", "ModelManager"]

import abc
import pathlib
from typing import Optional, Tuple

import torch
import torch.nn as nn

from utsadbench.core.data import SequenceCollection, TimeSeries


class Model(abc.ABC):
    """Abstraction of a Model/Method."""

    @abc.abstractmethod
    def do_train(self, sequences: SequenceCollection, **kwargs) -> "Model":
        """Run the training procedure

        Args:
            sequences: The SequenceCollection to use as the training dataset.

        Returns: The trained model.

        """
        pass

    @abc.abstractmethod
    def predict(
        self, sequences: SequenceCollection, **kwargs
    ) -> Tuple[TimeSeries, Optional[TimeSeries]]:
        """Run the prediction procedure.

        Args:
            sequences: The SequenceCollection to use as the test set.

        Returns: The anomaly score and optional the predicted time series.

        """
        pass

    @staticmethod
    @abc.abstractmethod
    def model_id(*args, **kwargs):
        """Get the model id

        Args:
            *args: The mandatory arguments to build the model id
            **kwargs: The optional argument for building the model id

        Returns: The model id

        """
        pass

    @staticmethod
    def load(path: str, device: torch.device = torch.device("cpu")) -> "Model":
        """Load a stored model

        Args:
            path: The path whete the model is stored.
            device: The device to load the model to. Default: cpu

        Returns: A Model

        """
        return ModelManager.load(path, device)

    def save(self, path: str, name: str) -> None:
        """Save a model to the given path.

        Args:
            path: The path to save the model at.
            name: The name to use as filename.

        """
        assert isinstance(self, nn.Module)
        ModelManager.save(self, path, name)


class ModelManager:
    @staticmethod
    def save(model: nn.Module, path: str, name: str):
        """Save a model to disk using the .pth format

        Args:
            model: The model to save
            path: the location to store the model at
            name: The filename

        """
        pathlib.Path(path).mkdir(parents=True, exist_ok=True)
        torch.save(model, f"{path}/{name}.pth")

    @staticmethod
    def load(path: str, device: torch.device = torch.device("cpu")) -> Model:
        """Load a model from the given path.

        Args:
            path: The path to load the model from
            device: The device to load the model to. efault: cpu

        Returns: Model

        """
        if not pathlib.Path(path).exists():
            raise RuntimeError(f"No model found for path {path}!")
        return torch.load(path, map_location=device)
