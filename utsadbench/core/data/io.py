# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import abc
import pathlib
from typing import Optional, Union

import pandas as pd  # type: ignore

import utsadbench.core.data.dataset as dataset


class TimeSeriesReader(abc.ABC):
    @staticmethod
    @abc.abstractmethod
    def from_file(
        file: pathlib.Path, fill_gaps: bool = False, subsample: Optional[str] = None
    ) -> dataset.TimeSeries:
        """Create a dataset.TimeSeries from data given in a file

        Args:
            file: The path to the file containing the data.
            fill_gaps: Whether to interpolate missing timestamps.
            subsample: Whether and how to subsample.

        Returns: dataset.TimeSeries

        """
        pass


def fill_gaps(
    df: pd.DataFrame,
    sampling_rate: str,
    value: Union[str, int, float],
    field: str = "X",
) -> pd.DataFrame:
    """Fill gaps in regular time series.

    Gaps in the regular timeseries provided in `df` will be filled with `value.

    Args:
        df: The dataframe containing the timeseries with the timestamps in column
            "timestamp" and the values in column `field`.
        sampling_rate:  The sampling rate of the timeseries given as pandas DateOffset
            string (see: https://pandas.pydata.org/pandas-docs/stable/user_\
guide/timeseries.html#dateoffset-objects)
        value:  The value to fill the gaps with.
        field:  The name of the column in the dataframe containing the values of
            the timeseries.

    Returns: pandas.DataFrame
    """
    df = df.resample(sampling_rate, on="timestamp").mean().reset_index()
    df[field] = df[field].fillna(value)
    return df


def subsample(df: pd.DataFrame, sampling_rate: str) -> pd.DataFrame:
    """Subsample regular time series.

    Subsample the timeseries given in `df` to a new sampling rate.

    Args:
        df: The dataframe containing the timeseries with the timestamps in column
            "timestamp" and the values in column `field.
        sampling_rate:  The sampling rate of the timeseries given as pandas DateOffset
            string (see: https://pandas.pydata.org/pandas-docs/stable/user_\
guide/timeseries.html#dateoffset-objects)

    Returns: pandas.DataFrame

    """
    df = df.resample(sampling_rate, on="timestamp").mean().reset_index()
    return df
