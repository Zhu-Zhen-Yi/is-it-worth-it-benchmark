# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from typing import List, Optional

from utsadbench.core.data import Normalizer, SequenceCollection, Sequencer, TimeSeries
from utsadbench.core.data.preprocessing import SequencePreprocessor


class DataPipeline:
    def __init__(
        self,
        data: TimeSeries,
        sequencer: Sequencer,
        normalizer: Optional[Normalizer] = None,
        preprocessors: Optional[List[SequencePreprocessor]] = None,
    ) -> None:
        self.data: TimeSeries = data
        self.sequencer = sequencer
        self.normalizer = normalizer
        self.preprocessors = preprocessors if preprocessors is not None else []

    def run(self) -> SequenceCollection:
        """Run the preprocessing timeline.

        The pipline consists of Sequencer [-> Normalizer] [-> Preprocessor]
        depending on the provided arguments


        Returns: SequenceCollection

        """
        if self.normalizer is not None:
            normalized = self.normalizer.normalize(self.data)
            assert isinstance(normalized, TimeSeries)
            result = self.sequencer.sequences(normalized)
        else:
            result = self.sequencer.sequences(self.data)

        for preprocessor in self.preprocessors:
            result = preprocessor.run(result)

        return result
