# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

__all__ = ["Dataset", "TimeSeries"]

from typing import List, Optional, Tuple

import numpy as np
import numpy.typing as npt

from utsadbench.core.utils import Collection


class Anomaly:
    def __init__(self, start: int, length: int):
        """Anomalous subsequence

        An Anomalous subsequence given as start and timestamp length.

        :param start: Start timestamp of the anomaly
        :param length: Length of the anomaly
        """
        self.start: int = start
        self.end: int = start + length - 1
        self.length: int = length


class TimeSeries:
    _min: Optional[float] = None
    _max: Optional[float] = None

    def __init__(
        self,
        data: npt.NDArray[np.floating],
        anomalies: Optional[List[Anomaly]] = None,
        name: str = "",
    ) -> None:
        """

        Args:
            data:
            anomalies:
            name:
        """
        """TimeSeries Constructor.

        Args:
            data: Multivatiate Time Series given as np.ndarray of shape (n, d)
                with n being the length of the time series and d it's dimension
            anomalies: Ground Truth Anomalies
            name: Name of the time series

        """
        self.data = data
        self.name = name
        self.length = data.shape[0]
        self.dim = data.shape[1]
        self.anomalies = anomalies if anomalies is not None else []
        self.n_anomalies = len(self.anomalies)
        self.labels = self.anomalies_to_labels(self.anomalies, self.length)

    @classmethod
    def from_numpy(
        cls,
        data: npt.ArrayLike,
        anomalies: Optional[List[Tuple[int, int]]] = None,
        name: str = "",
    ) -> "TimeSeries":

        """Create a TimeSeries from numpy array-like

        Args:
            data: shape: (n, d) length n and dimension d
            anomalies: Anomalies given as list of Tuples (s, l) with `s`
                being the start of the anomaly and `l` it's length
            name: Name of the time series.

        Returns: TimeSeries
        """
        anomalies = [] if anomalies is None else anomalies

        if isinstance(data, list):
            data = np.array(data)

        if not isinstance(data, np.ndarray):
            raise ValueError("'data' must be array-like!")

        if data.ndim > 2:
            raise ValueError("'data' must not have more than 2 dimensions!")

        try:
            data = data.astype(float)
        except ValueError:
            raise ValueError("'data' must have floating dtype!")

        if len(data.shape) == 1:
            data = np.expand_dims(data, axis=0)

        assert isinstance(data, np.ndarray)

        if not all(
            isinstance(a, tuple) and all(isinstance(i, int) for i in a)
            for a in anomalies
        ):
            raise ValueError("'anomalies' must be given as list of tuples.")
        anomalies_ = [Anomaly(s, l) for s, l in anomalies]

        return cls(data, anomalies_, name)

    @staticmethod
    def anomalies_to_labels(
        anomalies: List[Anomaly], length: int
    ) -> npt.NDArray[np.int_]:
        """Turn list of anomalies into binary labels

        Args:
            anomalies: List of Anomalies
            length: The length of the timeseries

        Returns: Binary anomaly labels
        """
        labels = np.zeros(length)
        for anomaly in anomalies:
            labels[anomaly.start : anomaly.end + 1] = 1

        return labels

    def is_anomalous(self) -> bool:
        """Returns whether the timeseries has at least one anomaly.

        Returns: True if timeseries contains >= 1 anomalies, False else-wise
        """
        return self.n_anomalies > 0

    @property
    def min(self):
        """
        Returns: Minimal value of the time series.
        """
        if self._min is None:
            self._min = np.min(self.data)

        return self._min

    @property
    def max(self):
        """
        Returns: Maximal value of the time series.
        """
        if self._max is None:
            self._max = np.max(self.data)

        return self._max

    def __len__(self):
        return self.length


class Dataset(Collection[TimeSeries]):
    """
    Might be obsolete and replaced by lazy loading Time Series
    """

    @classmethod
    def create(cls, items: List[TimeSeries], **kwargs) -> "Collection[TimeSeries]":
        is_time_series = all([isinstance(t, TimeSeries) for t in items])
        if not is_time_series:
            raise ValueError("'items' must be a list of TimeSeries!")

        return super().create(items, **kwargs)

    def add(self, item: TimeSeries) -> "Collection[TimeSeries]":
        if not isinstance(item, TimeSeries):
            raise ValueError("'item' must have type TimeSeries!")
        super(Dataset, self).add(item)

        return self
