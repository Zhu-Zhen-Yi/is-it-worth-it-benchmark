# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import abc

import numpy as np
import scipy.stats as spstats  # type: ignore

import utsadbench.core.data as data


class SequencePreprocessor(abc.ABC):
    @staticmethod
    @abc.abstractmethod
    def run(sequences: data.SequenceCollection, **kwargs):
        pass


class SequencesStats(SequencePreprocessor):
    @staticmethod
    def run(sequences: data.SequenceCollection, **kwargs) -> data.SequenceCollection:
        """Calculate statistical vector on each sequence.

        The seven dimensional vector contains:
            Minimum
            Maximum
            Mean
            StandardDeviation
            Skewness
            Kurtosis
            Variation

        Args:
            sequences: The SequenceCollection

        Returns: SequenceCollection

        """
        stats_sequences = []
        for sequence in sequences:
            stats = np.array(
                [
                    spstats.tmin(sequence.data)[0],
                    spstats.tmax(sequence.data)[0],
                    spstats.tmean(sequence.data),
                    spstats.tstd(sequence.data)[0],
                    spstats.skew(sequence.data)[0],
                    spstats.kurtosis(sequence.data)[0],
                    spstats.variation(sequence.data)[0],
                ]
            )
            stats_sequences.append(
                data.Sequence(
                    sequence.ts,
                    sequence.start,
                    sequence.length,
                    sequence.stride,
                    stats,
                    sequence.is_anomalous,
                )
            )

        return data.SequenceCollection.create(stats_sequences)
