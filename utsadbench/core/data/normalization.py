# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from typing import Union

import sklearn.preprocessing as preprocessing  # type: ignore

import utsadbench.core.data.dataset as dataset


class Normalizer:
    @staticmethod
    def normalize(
        origin: Union[dataset.TimeSeries, dataset.Dataset]
    ) -> Union[dataset.TimeSeries, dataset.Dataset]:
        """Normalize a TimeSeries or Dataset to [0, 1]

        Normalization Method: Min-Max-Scaling

        Args:
            origin: The unnormalizes TimeSeries or Dataset

        Returns: The normalized TimeSeries or Dataset

        """
        if isinstance(origin, dataset.TimeSeries):
            return Normalizer._normalize_time_series(origin)

        return Normalizer._normalize_dataset(origin)

    @staticmethod
    def _normalize_time_series(origin: dataset.TimeSeries) -> dataset.TimeSeries:
        """Normalize TimeSeries [0, 1]

        Normalization Method: Min-Max-Scaling

        Args:
            origin: The unnormalized Time Series

        Returns: The normalized Time Series

        """
        scaler = preprocessing.MinMaxScaler()
        scaler.fit(origin.data)
        normalized = scaler.transform(origin.data)

        return dataset.TimeSeries(normalized, origin.anomalies, name=origin.name)

    @staticmethod
    def _normalize_dataset(origin: dataset.Dataset) -> dataset.Dataset:
        """Normalize all TimeSeries in given Dataset [0, 1]

        Min-Max-Scaling is applied to each TimeSeries in the Dataset individually.

        Args:
            origin: The unnormalized Dataset

        Returns: The normalized Dataset

        """
        normalized = dataset.Dataset.empty()
        for ts in origin:
            normalized.add(Normalizer._normalize_time_series(ts))
        assert isinstance(normalized, dataset.Dataset)

        return normalized


class Denormalizer:
    @staticmethod
    def denormalize(
        normalized: Union[dataset.TimeSeries, dataset.Dataset],
        origin: Union[dataset.TimeSeries, dataset.Dataset],
    ) -> Union[dataset.TimeSeries, dataset.Dataset]:
        """Denormalize a normalized TimeSeries or Dataset

        Args:
            normalized: The normalized TimeSeries or Dataset
            origin:  The original TimeSeries or Dataset

        Returns: The denoemalized Time Series or Dataset

        Raises:
            ValueError if types mismatch
            ValueError if TimeSeries not in [0, 1]

        """
        if type(normalized) != type(origin):
            raise ValueError("'normalize' and 'origin' must have same type!")

        if isinstance(normalized, dataset.TimeSeries):
            assert isinstance(origin, dataset.TimeSeries)
            return Denormalizer._denormalize_time_series(normalized, origin)

        assert isinstance(origin, dataset.Dataset)
        return Denormalizer._denormalize_dataset(normalized, origin)

    @staticmethod
    def _denormalize_time_series(
        normalized: dataset.TimeSeries, origin: dataset.TimeSeries
    ) -> dataset.TimeSeries:
        """Denormalize a normalized TimeSeries

        Args:
            normalized: The normalized TimeSeries
            origin:  The original TimeSeries

        Returns: The denoemalized Time Series
        """
        scaler = preprocessing.MinMaxScaler()
        scaler.fit(origin.data)
        denormalized = scaler.inverse_transform(normalized.data)

        return dataset.TimeSeries(denormalized, origin.anomalies, name=origin.name)

    @staticmethod
    def _denormalize_dataset(
        normalized: dataset.Dataset, origin: dataset.Dataset
    ) -> dataset.Dataset:
        """Denormalize a normalized Dataset

        Args:
            normalized: The normalized Dataset
            origin:  The original Dataset

        Returns: The denoemalized Dataset

        Raises:
            ValueError if TimeSeries not in [0, 1]

        """
        denormalized = dataset.Dataset.empty()
        for n, o in zip(normalized, origin):
            denormalized.add(Denormalizer._denormalize_time_series(n, o))
        assert isinstance(denormalized, dataset.Dataset)

        return denormalized
