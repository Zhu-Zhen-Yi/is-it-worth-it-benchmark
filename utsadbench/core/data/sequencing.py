# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import warnings
from typing import List, Optional, Tuple, Union

import numpy as np
import numpy.typing as npt
import torch
import torch.utils.data as data
from sklearn.model_selection import train_test_split  # type: ignore

from utsadbench.core.data.dataset import TimeSeries
from utsadbench.core.utils import Collection


class Sequence:
    def __init__(
        self,
        ts: TimeSeries,
        start: int,
        length: int,
        stride: int,
        data: Optional[npt.NDArray[np.floating]] = None,
        is_anomalous: bool = False,
    ) -> None:
        """A Subsequecne of a time series.

        Args:
            ts: The TimeSeries the subsequence belongs to
            start: The start timestamp of the subsequence
            length: The length of the subsequence
            stride: The stride of the subsequence
            data: The data of the subsequence.
            is_anomalous: Whether the subsequence is anomalous or not.
        """
        self.ts: TimeSeries = ts
        self.start: int = start
        self.length: int = length
        self.stride: int = stride
        self.end: int = start + length
        self.is_anomalous = is_anomalous
        self._data = (
            data if data is not None else self.ts.data[start : start + length, :]
        )

    @property
    def data(self):
        return self._data

    def expand_dims(self, axis: Union[int, List[int]]) -> "Sequence":
        """Add dimension on given axis.

        Args:
            axis: The axis to add the dimension

        Returns: The modified Sequence object.

        """
        expanded = np.expand_dims(self._data, axis=axis)
        return Sequence(
            self.ts,
            self.start,
            self.length,
            self.stride,
            data=expanded,
            is_anomalous=self.is_anomalous,
        )

    def squeeze(self) -> "Sequence":
        """Reduce dimensions in Sequence.

        Returns:THe modified Sequence

        """
        squeezed = np.squeeze(self._data)
        return Sequence(
            self.ts,
            self.start,
            self.length,
            self.stride,
            data=squeezed,
            is_anomalous=self.is_anomalous,
        )


class SequenceCollection(Collection[Sequence]):
    @classmethod
    def create(cls, items: List[Sequence], **kwargs) -> "SequenceCollection":
        items_valid = all([isinstance(i, Sequence) for i in items])
        if not items_valid:
            raise ValueError("'items' must be Sequence objects in SequenceCollection")

        return cls(items)

    def add(self, item: Sequence) -> "SequenceCollection":
        if not isinstance(item, Sequence):
            raise ValueError("'item' must be of type Sequence")
        super(SequenceCollection, self).add(item)
        return self

    def split(
        self, test_ratio: float = 0.5, shuffle: bool = True, seed: Optional[bool] = None
    ) -> Tuple["SequenceCollection", "SequenceCollection"]:
        train_data, test_data = train_test_split(
            self.items, test_size=test_ratio, random_state=seed, shuffle=shuffle
        )

        train = self.create(train_data)
        test = self.create(test_data)

        return train, test


class TorchDataLoader:
    @staticmethod
    def unlabelled(
        sequences: Collection[Sequence], batch_size=16, shuffle=True
    ) -> data.DataLoader:
        """Create a torch.dataloader without labels from Sequence Collection
        Args:
            sequences: A Sequence collection
            batch_size: The mini-batch size.
            shuffle: Whether to shuffle the items in the dataloader.

        Returns: torch.data.DataLoader

        """
        numpy_data = np.array([s.data for s in sequences])
        tensors = torch.from_numpy(numpy_data).float()
        dataloader = data.DataLoader(
            dataset=tensors, batch_size=batch_size, shuffle=shuffle
        )

        return dataloader

    @staticmethod
    def labelled(sequences: Collection[Sequence], batch_size=1) -> data.DataLoader:
        """Create a torch.dataloader with labels from Sequence Collection

        The is_anomalous property of the Sequences will be used as labels.

        Args:
            sequences: A Sequence collection
            batch_size: The mini-batch size.

        Returns: torch.data.DataLoader

        """
        numpy_data = np.array([s.data for s in sequences])
        numpy_labels = np.array([s.is_anomalous for s in sequences])
        tensors = torch.from_numpy(numpy_data).float()
        labels = torch.from_numpy(numpy_labels)

        dataset = data.TensorDataset(tensors, labels)
        dataloader = data.DataLoader(dataset=dataset, batch_size=batch_size)

        return dataloader


class Sequencer:
    def __init__(
        self, sequence_length: int, stride: int, padding: bool = False
    ) -> None:
        """Constructor of the Sequencer object

        Args:
            sequence_length: The Subsequence length to apply.
            stride: The stride to use
            padding: Whether to apply padding to the time series
        """
        self.seq_len = sequence_length
        self.stride = stride
        self.padding = padding

    def sequences(self, ts: TimeSeries) -> SequenceCollection:
        """Generate sequences by applying a sliding window to the given time series.

        Args:
            ts: TimeSeries to apply the sliding window to.

        Returns: SequenceCollection

        """

        if self.padding and self.stride > 1:
            warnings.warn("Using Sequencer with padding and stride > 1.", UserWarning)
        data = ts.data
        labels = ts.labels
        k = ts.length - self.seq_len + 1
        start_index = 0
        if self.padding:
            pad = np.array([data[0, :] for i in range(self.seq_len)])
            data = np.concatenate([pad, data])
            labels = np.concatenate([np.ones(self.seq_len) * labels[0], labels])
            k = ts.length
            start_index = -self.seq_len

        sequences = SequenceCollection.empty()
        for i in np.arange(0, k, self.stride):
            sequence = Sequence(
                ts,
                i + start_index,
                self.seq_len,
                self.stride,
                data=data[i : i + self.seq_len, :],
                is_anomalous=labels[i : i + self.seq_len].sum() > 0,
            )
            sequences.add(sequence)

        assert isinstance(sequences, SequenceCollection)
        return sequences
