# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import logging
from typing import List

import hydra
import mlflow
import numpy as np
import torch
from omegaconf import DictConfig

import utsadbench.core as core
import utsadbench.core.data as data
import utsadbench.core.data.pipeline as pipeline
import utsadbench.core.data.preprocessing as preproc
import utsadbench.results as results
import utsadbench.utils as utils

logger = logging.getLogger(__name__)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def _seed(cfg: DictConfig):
    if "seed" in cfg and cfg.seed is not None:
        utils.Seeder.seed(cfg.seed)
        logger.info(f"Seeded RNGs with value {cfg.seed}")


def _run_ts(
    ts: data.TimeSeries,
    model: core.Model,
    sequencer: data.Sequencer,
    cfg: DictConfig,
    normalizer: data.Normalizer = None,
    preprocessors: List[preproc.SequencePreprocessor] = None,
):
    logger.info(f"{cfg.model.name} - {ts.name} started.")
    pipeline_ = pipeline.DataPipeline(
        ts, sequencer, normalizer=normalizer, preprocessors=preprocessors
    )
    sequences = pipeline_.run()
    train_sequences = sequences

    # 1. Train the model or load pretrained
    model_id = model.model_id(ts, device=device, **cfg.model.model_id)

    if cfg.retrain and cfg.model.trainable:
        _seed(cfg)
        model = model.do_train(
            train_sequences, device=device, **cfg.model.training_params
        )
    else:
        try:
            model = model.load(f"{cfg.model_dir}/{model_id}.pth", device=device)
        except RuntimeError:
            if cfg.model.trainable:
                logger.info(f"No model trained for: {model_id}! Training now!")
                _seed(cfg)
                model = model.do_train(
                    train_sequences, device=device, **cfg.model.training_params
                )

    # 2. Predict time series / compute anomaly scores
    _seed(cfg)
    result = model.predict(sequences, device=device, **cfg.model.predict_params)
    scores = result[0] if isinstance(result, tuple) else result
    if len(scores) != len(ts):
        logger.error(
            f"Scores and TimeSeries must have same length."
            f" Scores has length {len(scores)}, "
            f"should be {len(ts)}"
        )
        return

    prediction = result[1] if isinstance(result, tuple) else None
    if prediction is not None:
        if len(scores) != len(ts):
            logger.error(
                f"Prediction and TimeSeries must have same length."
                f" Prediction has length {len(prediction)}, "
                f"should be {len(ts)}"
            )
            return
        if normalizer is not None:
            prediction = data.Denormalizer.denormalize(prediction, ts)

    # 3. label anomalies
    classifier = hydra.utils.call(
        cfg.classifier.classifier, scores, ts, **cfg.classifier.classifier
    )
    if cfg.relabel:
        classifier.adjust_labels(backwards=cfg.relabel_backwards)

    # 4. calculate metrics
    auroc = results.AUROC.calculate(ts, classifier)
    f1 = results.F1.calculate(ts, classifier)
    ucrscore = results.UcrScore.calculate(ts, classifier)
    latency = results.Latency.calculate(ts, classifier)

    run_metrics = results.TimeSeriesMetrics.create(
        [auroc, f1, ucrscore, latency], name=ts.name
    )

    return run_metrics


@hydra.main(config_path="../../conf", config_name="config")
def main(cfg: DictConfig) -> None:
    dataset = hydra.utils.instantiate(cfg.dataset.dataset)
    model = hydra.utils.instantiate(cfg.model.model)
    sequencer = hydra.utils.instantiate(cfg.model.sequencer)
    normalizer = data.Normalizer if cfg.dataset.normalize else None
    preprocessors = hydra.utils.instantiate(cfg.model.preprocessors)

    utils.MLFlowHelper.init_experiment_from_config(cfg)

    with mlflow.start_run():
        # log param
        utils.MLFlowHelper.log_params_from_omegaconf_dict(cfg)

        # run jobs
        run_metrics = []
        for i in range(len(dataset)):
            try:
                ts = dataset[i]
                run_metrics.append(
                    _run_ts(ts, model, sequencer, cfg, normalizer, preprocessors)
                )
            except ValueError:
                run_metrics.append(
                    results.TimeSeriesMetrics.create(
                        [
                            results.AUROC(0.0, results.AUROC.NAME),
                            results.F1(0.0, results.F1.NAME),
                            results.UcrScore(0, results.UcrScore.NAME),
                            results.Latency(-10000, results.Latency.NAME),
                        ],
                        name=ts.name,
                    )
                )

        # aggregate metrics
        means = {}
        for m in run_metrics:
            if m is None:
                logger.error("Error when running Timeseries.")
                continue
            for i in m.items:
                if i.name not in means.keys():
                    means[i.name] = []
                means[i.name].append(i.value)

        for name, values in means.items():
            mlflow.log_metric(name, np.mean(values))

    return np.mean(means[cfg.hyperopt.objective])


if __name__ == "__main__":
    main()
