# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import logging
from typing import List

import hydra
import mlflow
import torch
from omegaconf import DictConfig

import utsadbench.core as core
import utsadbench.core.data as data
import utsadbench.core.data.pipeline as pipeline
import utsadbench.core.data.preprocessing as preproc
import utsadbench.results as results
import utsadbench.utils as utils

logger = logging.getLogger(__name__)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def _seed(cfg: DictConfig):
    if "seed" in cfg and cfg.seed is not None:
        utils.Seeder.seed(cfg.seed)
        logger.info(f"Seeded RNGs with value {cfg.seed}")


def _run_ts(
    ts: data.TimeSeries,
    model: core.Model,
    sequencer: data.Sequencer,
    plotter: results.plots.AnomalyDetectionPlot,
    cfg: DictConfig,
    normalizer: data.Normalizer = None,
    preprocessors: List[preproc.SequencePreprocessor] = None,
):
    logger.info(f"{cfg.model.name} - {ts.name} started.")
    pipeline_ = pipeline.DataPipeline(
        ts, sequencer, normalizer=normalizer, preprocessors=preprocessors
    )
    sequences = pipeline_.run()
    train_sequences = sequences

    # 1. Train the model or load pretrained
    model_id = model.model_id(ts, device=device, **cfg.model.model_id)
    with mlflow.start_run():
        # log params
        mlflow.set_tag("timeseries", ts.name)
        utils.MLFlowHelper.log_params_from_omegaconf_dict(cfg)

        if cfg.retrain and cfg.model.trainable:
            _seed(cfg)
            model = model.do_train(
                train_sequences, device=device, **cfg.model.training_params
            )
        else:
            try:
                model = model.load(f"{cfg.model_dir}/{model_id}.pth", device=device)
            except RuntimeError:
                if cfg.model.trainable:
                    logger.info(f"No model trained for: {model_id}! Training now!")
                    _seed(cfg)
                    model = model.do_train(
                        train_sequences, device=device, **cfg.model.training_params
                    )

        # 2. Predict time series / compute anomaly scores
        _seed(cfg)
        result = model.predict(sequences, device=device, **cfg.model.predict_params)
        scores = result[0] if isinstance(result, tuple) else result
        if len(scores) != len(ts):
            logger.error(
                f"Scores and TimeSeries must have same length."
                f" Scores has length {len(scores)}, "
                f"should be {len(ts)}"
            )
            return
        if "scores" in cfg.store_intermediate_results:
            results.PickleExporter.export(scores, "scores", model_id, cfg)

        prediction = result[1] if isinstance(result, tuple) else None
        if prediction is not None:
            if len(scores) != len(ts):
                logger.error(
                    f"Prediction and TimeSeries must have same length."
                    f" Prediction has length {len(prediction)}, "
                    f"should be {len(ts)}"
                )
                return
            if normalizer is not None:
                prediction = data.Denormalizer.denormalize(prediction, ts)
        if "prediction" in cfg.store_intermediate_results and prediction is not None:
            results.PickleExporter.export(prediction, "prediction", model_id, cfg)

        # 3. label anomalies
        classifier = hydra.utils.call(
            cfg.classifier.classifier, scores, ts, **cfg.classifier.classifier
        )
        if "classifier" in cfg.store_intermediate_results:
            results.PickleExporter.export(classifier, "classifier", model_id, cfg)

        if cfg.relabel:
            classifier.adjust_labels(backwards=cfg.relabel_backwards)

        # 4. calculate metrics
        auroc = results.AUROC.calculate(ts, classifier)
        f1 = results.F1.calculate(ts, classifier)
        ucrscore = results.UcrScore.calculate(ts, classifier)
        latency = results.Latency.calculate(ts, classifier)

        run_metrics = results.TimeSeriesMetrics.create(
            [auroc, f1, ucrscore, latency], name=ts.name
        )
        run_metrics.to_mlfow()

        # 5. create plots
        plotter.to_mlflow(ts=ts, classifier=classifier, prediction=prediction)


@hydra.main(config_path="../../conf", config_name="config")
def main(cfg: DictConfig) -> None:
    dataset = hydra.utils.instantiate(cfg.dataset.dataset)
    model = hydra.utils.instantiate(cfg.model.model)
    sequencer = hydra.utils.instantiate(cfg.model.sequencer)
    normalizer = data.Normalizer if cfg.dataset.normalize else None
    preprocessors = hydra.utils.instantiate(cfg.model.preprocessors)
    plotter = results.AnomalyDetectionPlot(cfg.result_dir)

    utils.MLFlowHelper.init_experiment_from_config(cfg)

    for i in range(len(dataset)):
        try:
            ts = dataset[i]
        except RuntimeError as re:
            logger.exception(re)
            continue
        except ValueError as ve:
            logger.exception(ve)
            continue

        try:
            _run_ts(ts, model, sequencer, plotter, cfg, normalizer, preprocessors)
        except ValueError as ve:
            logger.exception(ve)
            zero_metrics = results.TimeSeriesMetrics.create(
                [
                    results.AUROC(0.0, results.AUROC.NAME),
                    results.F1(0.0, results.F1.NAME),
                    results.UcrScore(0, results.UcrScore.NAME),
                    results.Latency(-10000, results.Latency.NAME),
                ],
                name=ts.name,
            )
            with mlflow.start_run():
                zero_metrics.to_mlfow()
                mlflow.set_tag("timeseries", ts.name)
                mlflow.set_tag("failed", "1")
                utils.MLFlowHelper.log_params_from_omegaconf_dict(cfg)


if __name__ == "__main__":
    main()
