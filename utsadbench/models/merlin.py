# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from typing import Optional, Tuple

import merlin
import numpy as np

import utsadbench.core.data as data
import utsadbench.core.model as model


class Sequencer(data.Sequencer):
    def sequences(self, ts: data.TimeSeries) -> data.SequenceCollection:
        self.seq_len = len(ts)
        return super().sequences(ts)


class Merlin(model.Model):
    def do_train(self, sequences: data.SequenceCollection, **kwargs) -> "Merlin":
        """MERLIN dos not have a training step."""
        return self

    def predict(
        self, sequences: data.SequenceCollection, **kwargs
    ) -> Tuple[data.TimeSeries, Optional[data.TimeSeries]]:
        time_series = sequences[0].ts
        a_lengths = [a.length for a in time_series.anomalies]

        l_min = (
            kwargs["l_min"]
            if "l_min" in kwargs and kwargs["l_min"] is not None
            else int(min(a_lengths) * 0.75)
        )
        l_max = (
            kwargs["l_max"]
            if "l_max" in kwargs and kwargs["l_min"] is not None
            else int(max(a_lengths) * 1.25)
        )
        l_min = 4 if l_min < 4 else l_min
        l_max = 5 if l_max < 5 else l_max

        discords, distances, lengths = merlin.merlin(
            time_series.data.transpose(), l_min, l_max
        )
        scores = np.zeros(sequences[0].ts.length)
        scores[discords] = distances

        return data.TimeSeries.from_numpy(scores.reshape((-1, 1))), None

    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs):
        return f"merlin_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"
