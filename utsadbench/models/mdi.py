# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np

import utsadbench.core.data as data
import utsadbench.results.classifier as classifier
from lib.libmaxdiv.maxdiv import maxdiv
from utsadbench.core.model import Model
from utsadbench.core.utils import InflatableEnum


class MDIMethod(InflatableEnum):
    GAUSSIAN_COV = "gaussian_cov"
    GAUSSIAN_GLOBAL_COV = "gaussian_global_cov"
    GAUSSIAN_ID_COV = "gaussian_id_cov"
    PARZEN = "parzen"
    ERPH = "erph"


class MDIProposals(InflatableEnum):
    DENSE = "dense"
    HOTELLINGS_T = "hotellings_t"
    KDE = "kde"


class MDIMode(InflatableEnum):
    KL_I_OMEGA = "I_OMEGA"
    KL_OMEGA_I = "OMEGA_I"
    KL_SYM = "SYM"
    KL_TS = "TS"
    CROSSENT = "CROSSENT"
    JSD = "JSD"


class MDIPreprocessing(InflatableEnum):
    TD = "td"


class MDIClassifier(classifier.AbstractAnomalyClassifier):
    @staticmethod
    def calculate_threshold(scores: data.TimeSeries, **kwargs) -> float:
        th = np.min(scores.data[np.nonzero(scores.data)])
        return th.min()


class MDI(Model):
    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs) -> str:
        return f"mdi_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"

    def __init__(
        self,
        method: MDIMethod = MDIMethod.GAUSSIAN_COV,
        mode: MDIMode = MDIMode.KL_TS,
        proposals: MDIProposals = MDIProposals.DENSE,
        preprocessing: MDIPreprocessing = None,
    ) -> None:
        self.method = method
        self.mode = mode
        self.proposals = proposals
        self.preprocessing = preprocessing if preprocessing is not None else []

    def do_train(self, sequences: data.SequenceCollection, **kwargs) -> "MDI":
        """MDI dos not have a training step."""
        return self

    def predict(self, sequences: data.SequenceCollection, **kwargs) -> data.TimeSeries:
        time_series = sequences[0].ts
        a_lengths = [a.length for a in time_series.anomalies]

        num_intervals = kwargs["num_intervals"] if "num_intervals" in kwargs else None
        l_min = (
            kwargs["l_min"]
            if "l_min" in kwargs and kwargs["l_min"] is not None
            else int(min(a_lengths) * 0.75)
        )
        l_max = (
            kwargs["l_max"]
            if "l_max" in kwargs and kwargs["l_max"] is not None
            else int(max(a_lengths) * 1.25)
        )

        detections = maxdiv.maxdiv(
            sequences[0].data.transpose(),
            method=self.method,
            num_intervals=num_intervals,
            useLibMaxDiv=True,
            mode=self.mode,
            proposals=self.proposals,
            extint_min_len=l_min,
            extint_max_len=l_max,
            preproc=self.preprocessing,
            td_dim=0 if self.preprocessing == str(MDIPreprocessing.TD) else None,
            td_lag=0 if self.preprocessing == str(MDIPreprocessing.TD) else None,
        )

        scores = np.zeros(len(time_series))
        for start, end, score in detections:
            scores[start:end] = score

        return data.TimeSeries.from_numpy(scores.reshape(-1, 1))
