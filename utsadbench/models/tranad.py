# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import math
from functools import partial
from typing import Optional, Tuple

import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm

import lib.tranad.main as tranad
import lib.tranad.src.dlutils as dlutils
import utsadbench.core.data as data
import utsadbench.core.model as model
from utsadbench.core.data import SequenceCollection


class TranADModel(nn.Module):
    def __init__(
        self, n_feats: int, lr=0.006, batch_size: int = 128, n_window: int = 100
    ):
        super(TranADModel, self).__init__()
        self.name = "TranAD"
        self.lr = lr
        self.batch = batch_size
        self.n_feats = n_feats
        self.n_window = n_window
        self.n = self.n_feats * self.n_window
        self.pos_encoder = dlutils.PositionalEncoding(2 * n_feats, 0.1, self.n_window)
        encoder_layers = dlutils.TransformerEncoderLayer(
            d_model=2 * n_feats, nhead=n_feats, dim_feedforward=16, dropout=0.1
        )
        self.transformer_encoder = nn.TransformerEncoder(encoder_layers, 1)
        decoder_layers1 = dlutils.TransformerDecoderLayer(
            d_model=2 * n_feats, nhead=n_feats, dim_feedforward=16, dropout=0.1
        )
        self.transformer_decoder1 = nn.TransformerDecoder(decoder_layers1, 1)
        decoder_layers2 = dlutils.TransformerDecoderLayer(
            d_model=2 * n_feats, nhead=n_feats, dim_feedforward=16, dropout=0.1
        )
        self.transformer_decoder2 = nn.TransformerDecoder(decoder_layers2, 1)
        self.fcn = nn.Sequential(nn.Linear(2 * n_feats, n_feats), nn.Sigmoid())

    def encode(self, src, c, tgt):
        src = torch.cat((src, c), dim=2)
        src = src * math.sqrt(self.n_feats)
        src = self.pos_encoder(src)
        memory = self.transformer_encoder(src)
        tgt = tgt.repeat(1, 1, 2)
        return tgt, memory

    def forward(self, src, tgt):
        # Phase 1 - Without anomaly scores
        c = torch.zeros_like(src)
        x1 = self.fcn(self.transformer_decoder1(*self.encode(src, c, tgt)))
        # Phase 2 - With anomaly scores
        c = (x1 - src) ** 2
        x2 = self.fcn(self.transformer_decoder2(*self.encode(src, c, tgt)))
        return x1, x2


class TranAD(model.Model, TranADModel):
    def __init__(
        self,
        optimizer: partial[torch.optim.Optimizer],
        scheduler: partial[torch.optim.lr_scheduler.StepLR],
        n_feats: int,
        lr=0.006,
        batch_size: int = 128,
        sequence_length: int = 10,
    ) -> None:
        super(TranAD, self).__init__(
            n_feats, lr=lr, batch_size=batch_size, n_window=sequence_length
        )
        self.optimizer_partial: partial[torch.optim.Optimizer] = optimizer
        self.scheduler_partial: partial[torch.optim.lr_scheduler.StepLR] = scheduler
        self.optimizer: Optional[torch.optim.Optimizer] = None
        self.scheduler: Optional[torch.optim.lr_scheduler.StepLR] = None

    def do_train(self, sequences: SequenceCollection, **kwargs) -> "TranAD":
        device = kwargs["device"] if "device" in kwargs else torch.device("cpu")
        self.to(device=device, dtype=torch.double)
        self.optimizer = self.optimizer_partial(self.parameters())
        self.scheduler = self.scheduler_partial(self.optimizer)

        sequences_np = np.array([s.data for s in sequences])
        # sequences_np = np.swapaxes(sequences_np, 1, 2)

        epochs = kwargs["epochs"] if "epochs" in kwargs else 1
        savepath = kwargs["savepath"] if "savepath" in kwargs else None
        supervision = kwargs["supervision"] if "supervision" in kwargs else ""

        accuracy_list = []
        for epoch in tqdm(range(epochs)):
            lossT, lr = tranad.backprop(
                epoch,
                self,
                sequences_np,
                torch.ones((1, sequences[0].ts.dim)),
                self.optimizer,
                self.scheduler,
            )
            accuracy_list.append((lossT, lr))

        accuracy_list.append((lossT, lr))

        if savepath is not None:
            model.ModelManager.save(
                self,
                savepath,
                self.model_id(
                    ts=sequences[0].ts,
                    device=device,
                    seed=kwargs["seed"],
                    lr=self.lr,
                    step_size=self.scheduler.state_dict()["step_size"],
                    gamma=self.scheduler.state_dict()["gamma"],
                    nfeats=self.n_feats,
                    batch_size=self.batch,
                    epochs=epochs,
                    sequence_length=sequences[0].length,
                    stride=sequences[0].stride,
                    supervision=supervision,
                ),
            )

        return self

    def predict(
        self, sequences: SequenceCollection, **kwargs
    ) -> Tuple[data.TimeSeries, data.TimeSeries]:
        device = kwargs["device"] if "device" in kwargs else torch.device("cpu")
        torch.zero_grad = True
        self.eval()

        self.to(device=device, dtype=torch.double)
        sequences_np = np.array([s.data for s in sequences])
        loss, y_pred = tranad.backprop(
            0,
            self,
            sequences_np,
            torch.ones((1, self.n_feats)),
            self.optimizer,
            self.scheduler,
            training=False,
        )
        scores = np.mean(loss, axis=1)

        scores = np.array(
            [np.full(sequences[0].stride, val) for val in scores]
        ).flatten()
        y_pred = np.array(
            [np.full((sequences[0].stride, self.n_feats), val) for val in y_pred]
        )
        y_pred = np.squeeze(y_pred) if self.n_feats > 1 else y_pred.reshape((-1, 1))

        return (
            data.TimeSeries.from_numpy(scores.reshape((-1, 1))),
            data.TimeSeries.from_numpy(y_pred),
        )

    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs) -> str:
        return f"tranad_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"
