# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from typing import Tuple

import numpy as np
import torch
import torch.nn as nn
from torch.nn.init import xavier_uniform_
from torch.nn.utils import clip_grad_value_
from tqdm import tqdm

import lib.GANF.models.GANF as ganf
import utsadbench.core.data as data
import utsadbench.core.model as coremodel


class GANF(coremodel.Model, ganf.GANF):
    def __init__(
        self, input_shape: int, latent_dim: int, num_blocks: int, num_hidden: int
    ):
        self.A = None

        self.input_shape = input_shape
        self.latent_dim = latent_dim
        self.num_blocks = num_blocks
        self.num_hidden = num_hidden
        ganf_kwargs = {
            "n_blocks": num_blocks,
            "input_size": input_shape,
            "hidden_size": latent_dim,
            "n_hidden": num_hidden,
            "dropout": 0.1,
            "model": "MAF",
            "batch_norm": True,
        }

        super(GANF, self).__init__(**ganf_kwargs)

    def test(self, x, A):
        return super().test(x, A)

    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs) -> str:
        return f"ganf_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"

    @staticmethod
    def load(path: str, device: torch.device = torch.device("cpu")) -> "GANF":
        pathparts = path.split(".")

        model = super(GANF, GANF).load(path, device)
        model_id = ".".join(pathparts[0:-1])
        model.A = coremodel.ModelManager.load(
            f"{model_id}_graph.{pathparts[-1]}", device
        )

        assert isinstance(model, GANF)

        return model

    def do_train(self, sequences: data.SequenceCollection, **kwargs) -> "GANF":
        """

        Args:
            sequences: utsadbench.core.data.SequenceCollection
                The training sequences
            **kwargs:
                batch_size: int
                    The batch size used for training.
                    Default: 1
                epochs: int
                     The number of training epochs.
                     Default: 1
                savepath: str
                    The path to store the trained model.
                    If ``savepath is None``, the trained model will not be stored.
                    Default: None
                seed: int
                    The random seed used to perform the training test split.
                    Default: None
                device: torch.device
                    The device to run the training on. Options: ``cpu`` or ``cuda``.
                    Default: torch.device("cpu")
                lr_init: float
                    The initial learning rate used for training

        Returns: GANF
            The trained model, having matrix A set as a property

        """
        batch_size = kwargs["batch_size"] if "batch_size" in kwargs else 1
        seed = kwargs["seed"] if "seed" in kwargs else None
        savepath = kwargs["savepath"] if "savepath" in kwargs else None
        epochs = kwargs["epochs"] if "epochs" in kwargs else 1
        device = torch.device(kwargs["device"] if "device" in kwargs else "cpu")
        lr_init = kwargs["lr_init"] if "lr_init" in kwargs else 2e-3
        supervision = kwargs["supervision"] if "supervision" in kwargs else ""

        self.A = GANFTrainer.train(
            self,
            sequences,
            savepath=savepath,
            batch_size=batch_size,
            epochs=epochs,
            device=device,
            seed=seed,
            lr_init=lr_init,
            supervision=supervision,
        )

        return self

    def predict(self, sequences: data.SequenceCollection, **kwargs) -> data.TimeSeries:
        device = torch.device(kwargs["device"] if "device" in kwargs else "cpu")

        sequences = data.SequenceCollection(
            [s.expand_dims(axis=(0, 1)) for s in sequences]
        )
        dataloader = data.TorchDataLoader.labelled(sequences)

        scores = []
        with torch.no_grad():
            self.eval()
            for x, _ in tqdm(dataloader):
                x = x.to(device)
                loss = -self.test(x, self.A.data).cpu().numpy()
                scores.append(loss)

        scores = np.array(
            [np.full(sequences[0].stride, val) for val in scores]
        ).flatten()

        # adjust length to length of time series
        pad_length = sequences[0].ts.length - scores.shape[0]
        scores = np.concatenate([scores, np.ones(pad_length) * scores[-1]])
        return data.TimeSeries.from_numpy(scores.reshape((-1, 1)))


class GANFTrainer:
    WEIGHT_DECAY = 5e-4
    MAX_ITER = 20
    H_TOL = 1e-4
    RHO_INIT = 1.0
    RHO_MAX = 1e16
    LAMBDA1 = 0.0
    ALPHA_INIT = 0.0
    N_SENSOR = 1
    LOG_INTERVAL = 5

    @staticmethod
    def train(
        model: GANF,
        sequences: data.SequenceCollection,
        batch_size: int,
        savepath: str = None,
        epochs: int = 1,
        device: torch.device = torch.device("cpu"),
        seed: int = None,
        lr_init: float = 2e-3,
        supervision="",
    ) -> Tuple[coremodel.Model, nn.Module]:
        modelname = model.model_id(
            sequences[0].ts,
            device=device,
            epochs=epochs,
            seed=seed,
            latent_dim=model.latent_dim,
            num_blocks=model.num_blocks,
            num_hidden=model.num_hidden,
            sequence_length=sequences[0].length,
            stride=sequences[0].stride,
            lr_init=lr_init,
            supervision=supervision,
        )
        sequences = data.SequenceCollection(
            [s.expand_dims(axis=[0, 1]) for s in sequences]
        )
        train, val = sequences.split(test_ratio=0.15, shuffle=True, seed=seed)
        train_loader = data.TorchDataLoader.unlabelled(
            train, batch_size=batch_size, shuffle=True
        )
        val_loader = data.TorchDataLoader.unlabelled(
            val, batch_size=batch_size, shuffle=True
        )
        model = model.to(device)

        loss_best = np.inf
        rho = GANFTrainer.RHO_INIT
        epoch = 0
        lr = lr_init
        n_sensor = GANFTrainer.N_SENSOR
        alpha = GANFTrainer.ALPHA_INIT
        h_A_old = np.inf

        # initialize A
        init = torch.zeros([n_sensor, n_sensor])
        init = xavier_uniform_(init).abs()
        init = init.fill_diagonal_(0.0)
        A = torch.tensor(init, requires_grad=True, device=device)

        for _ in tqdm(range(GANFTrainer.MAX_ITER)):
            while rho < GANFTrainer.RHO_MAX:
                optimizer = torch.optim.Adam(
                    [
                        {
                            "params": model.parameters(),
                            "weight_decay": GANFTrainer.WEIGHT_DECAY,
                        },
                        {"params": [A]},
                    ],
                    lr=lr,
                    weight_decay=0.0,
                )

                for _ in tqdm(range(epochs)):
                    epoch += 1
                    h, rho, A, alpha, _ = GANFTrainer._train_epoch(
                        model,
                        optimizer,
                        train_loader,
                        val_loader,
                        A,
                        rho,
                        alpha,
                        n_sensor,
                        epoch,
                        device,
                    )
                print(f"rho: {rho}, alpha {alpha}, h {h.item()}")
                print("===========================================")
                if savepath is not None:
                    coremodel.ModelManager.save(A.data, savepath, f"{modelname}_graph")
                    coremodel.ModelManager.save(model, savepath, f"{modelname}")

                del optimizer
                torch.cuda.empty_cache()

                if h.item() > 0.5 * h_A_old:
                    rho *= 10
                else:
                    break

            h_A_old = h.item()
            alpha += rho * h.item()

            if h_A_old <= GANFTrainer.H_TOL or rho >= GANFTrainer.RHO_MAX:
                break

        lr = lr_init
        optimizer = torch.optim.Adam(
            [
                {
                    "params": model.parameters(),
                    "weight_decay": GANFTrainer.WEIGHT_DECAY,
                },
                {"params": [A]},
            ],
            lr=lr,
            weight_decay=0.0,
        )
        for _ in tqdm(range(30)):
            epoch += 1
            h, rho, A, alpha, loss_val = GANFTrainer._train_epoch(
                model,
                optimizer,
                train_loader,
                val_loader,
                A,
                rho,
                alpha,
                n_sensor,
                epoch,
                device,
            )
            if np.mean(loss_val) < loss_best:
                loss_best = np.mean(loss_val)
                if savepath is not None:
                    print(f"save model {epoch} epoch")
                    coremodel.ModelManager.save(A.data, savepath, f"{modelname}_graph")
                    coremodel.ModelManager.save(model, savepath, f"{modelname}")

        return A

    @staticmethod
    def _train_epoch(
        model,
        optimizer,
        train_loader,
        val_loader,
        A,
        rho,
        alpha,
        n_sensor,
        epoch,
        device=torch.device("cpu"),
    ):
        # train iteration
        loss_train = []
        model.train()
        for x in tqdm(train_loader):
            x = x.to(device)

            optimizer.zero_grad()
            loss = -model(x, A)
            h = torch.trace(torch.matrix_exp(A * A)) - n_sensor
            total_loss = loss + 0.5 * rho * h * h + alpha * h

            total_loss.backward()
            clip_grad_value_(model.parameters(), 1)
            optimizer.step()
            loss_train.append(loss.item())
            A.data.copy_(torch.clamp(A.data, min=0, max=1))

        # evaluate iteration
        model.eval()
        loss_val = []
        with torch.no_grad():
            for x in tqdm(val_loader):
                x = x.to(device)
                loss = -model.test(x, A.data).cpu().numpy()
                loss_val.append(loss)
        loss_val = np.concatenate(loss_val)

        loss_val = np.nan_to_num(loss_val)
        print(
            f"'Epoch: {epoch}, train -log_prob: {np.mean(loss_train):.2f},"
            f" val -log_prob: {np.mean(loss_val):.2f}, h: {h.item()}"
        )

        return h, rho, A, alpha, loss_val
