# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

__all__ = [
    "DenseAutoencoder",
]

import time
from functools import partial
from typing import Optional, Tuple, Union

import numpy as np
import torch
import torch.nn as nn
from tqdm import tqdm

import utsadbench.core.data as data
import utsadbench.core.model as model
from utsadbench.core.data import SequenceCollection


class DenseEncoder(nn.Module):
    def __init__(self, input_shape: int, latent_dim: int):
        super().__init__()
        self.l1 = nn.Linear(in_features=input_shape, out_features=4 * latent_dim)
        self.l2 = nn.Linear(in_features=4 * latent_dim, out_features=2 * latent_dim)
        self.l3 = nn.Linear(in_features=2 * latent_dim, out_features=latent_dim)

    def forward(self, inputs: torch.Tensor) -> torch.Tensor:
        x = self.l1(inputs)
        x = torch.relu(x)
        x = self.l2(x)
        x = torch.relu(x)
        x = self.l3(x)
        latent = torch.relu(x)
        return latent


class DenseDecoder(nn.Module):
    def __init__(self, output_shape: int, latent_dim: int):
        super().__init__()
        self.l4 = nn.Linear(in_features=latent_dim, out_features=2 * latent_dim)
        self.l5 = nn.Linear(in_features=2 * latent_dim, out_features=4 * latent_dim)
        self.output = nn.Linear(in_features=4 * latent_dim, out_features=output_shape)

    def forward(self, latent: torch.Tensor) -> torch.Tensor:
        x = self.l4(latent)
        x = torch.relu(x)
        x = self.l5(x)
        x = torch.relu(x)
        output = self.output(x)

        return output


class DenseAutoencoderModel(nn.Module):
    def __init__(self, input_shape, latent_dim):
        super().__init__()
        self.encoder = DenseEncoder(input_shape, latent_dim)
        self.decoder = DenseDecoder(input_shape, latent_dim)
        self.latent_dim = latent_dim

    def forward(self, inputs):
        latent = self.encoder(inputs)
        output = self.decoder(latent)
        return output


class DenseAutoencoder(model.Model, DenseAutoencoderModel):
    def __init__(
        self,
        input_shape: Union[int, Tuple[int, int]],
        latent_dim: int,
        criterion: nn.Module,
        optimizer: partial[torch.optim.Optimizer],
    ):
        super(DenseAutoencoder, self).__init__(input_shape, latent_dim)
        self.optimizer_partial: partial[torch.optim.Optimizer] = optimizer
        self.optimizer: Optional[torch.optim.Optimizer] = None
        self.criterion: nn.Module = criterion

    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs) -> str:
        return f"denseae_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"

    def do_train(self, sequences: SequenceCollection, **kwargs) -> "DenseAutoencoder":
        device = kwargs["device"] if "device" in kwargs else torch.device("cpu")
        epochs = kwargs["epochs"] if "epochs" in kwargs else 1
        batch_size = kwargs["batch_size"] if "batch_size" in kwargs else 1
        seed = kwargs["seed"] if "seed" in kwargs else None
        savepath = kwargs["savepath"] if "savepath" in kwargs else None
        quiet = kwargs["quiet"] if "quiet" in kwargs else False

        self.to(device=device)
        self.optimizer = self.optimizer_partial(self.parameters())

        sequences = data.SequenceCollection([s.squeeze() for s in sequences])

        train, val = sequences.split(test_ratio=0.15, shuffle=True, seed=seed)
        train_loader = data.TorchDataLoader.unlabelled(
            train, batch_size=batch_size, shuffle=True
        )
        val_loader = data.TorchDataLoader.unlabelled(
            val, batch_size=batch_size, shuffle=True
        )

        history = dict(train=[], val=[])
        best_model = self
        best_train_loss = 1e20
        best_val_loss = 1e20
        best_epoch = -1
        for epoch in range(1, epochs + 1):
            start = time.time()
            self.train()

            train_losses = []
            for seq_true in tqdm(train_loader, disable=quiet):
                self.optimizer.zero_grad()
                seq_true = seq_true.to(device)
                seq_pred = self(seq_true)
                loss = self.criterion(seq_pred, seq_true)
                loss.backward()
                self.optimizer.step()
                train_losses.append(loss.item())

            val_losses = []
            self.eval()
            with torch.no_grad():
                for seq_true in tqdm(val_loader, disable=quiet):
                    seq_true = seq_true.to(device)
                    seq_pred = self(seq_true)

                    loss = self.criterion(seq_pred, seq_true)
                    val_losses.append(loss.item())

            train_loss = np.mean(train_losses)
            val_loss = np.mean(val_losses)

            history["train"].append(train_loss)
            history["val"].append(val_loss)

            if train_loss + val_loss < best_train_loss + best_val_loss:
                best_train_loss = train_loss
                best_val_loss = val_loss
                best_model = self
                best_epoch = epoch

            end = time.time()
            if not quiet:
                print(
                    f"Epoch {epoch}: train loss {train_loss}, val loss {val_loss}, "
                    f"took {end - start} seconds",
                    flush=True,
                )
        if not quiet:
            print(
                f"Best Epoch: {best_epoch}, best train loss: {best_train_loss},"
                f" best cval loss: {best_val_loss}"
            )

        if savepath is not None:
            model.ModelManager.save(
                best_model,
                savepath,
                self.model_id(
                    ts=sequences[0].ts,
                    device=device,
                    latent_dim=self.latent_dim,
                    seed=seed,
                    batch_size=batch_size,
                    epochs=epochs,
                    sequence_length=sequences[0].length,
                    stride=sequences[0].stride,
                ),
            )

        return best_model.eval()

    def predict(
        self, sequences: data.SequenceCollection, **kwargs
    ) -> Tuple[data.TimeSeries, data.TimeSeries]:
        device = torch.device(kwargs["device"] if "device" in kwargs else "cpu")
        quiet = kwargs["quiet"] if "quiet" in kwargs else False

        sequences = data.SequenceCollection([s.squeeze() for s in sequences])
        dataloader = data.TorchDataLoader.labelled(sequences)

        predictions, losses = [], []
        with torch.no_grad():
            self.eval()
            for i, (seq_true, label) in enumerate(tqdm(dataloader, disable=quiet)):
                seq_true = seq_true.to(device)
                seq_pred = self(seq_true)

                loss = self.criterion(seq_pred, seq_true)

                predictions.append(seq_pred.cpu().numpy().flatten())
                losses.append(loss.item())

        scores = np.array(
            [np.full(sequences[0].stride, val) for val in losses]
        ).flatten()

        predictions = np.array(
            [np.full(sequences[0].stride, val) for val in predictions]
        ).flatten()

        # adjust length to length of time series
        pad_length = sequences[0].ts.length - scores.shape[0]
        scores = np.concatenate([scores, np.ones(pad_length) * scores[-1]])
        predictions = np.concatenate(
            [predictions, np.ones(pad_length) * predictions[-1]]
        )

        return data.TimeSeries.from_numpy(
            scores.reshape((-1, 1))
        ), data.TimeSeries.from_numpy(predictions.reshape((-1, 1)))
