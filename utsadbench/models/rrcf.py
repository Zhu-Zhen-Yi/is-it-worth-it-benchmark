# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import numpy as np
import pandas as pd
import rrcf

import utsadbench.core.data as data
import utsadbench.core.model as model
from utsadbench.core.data import Sequencer as CoreSequencer


class Sequencer(CoreSequencer):
    def sequences(self, ts: data.TimeSeries) -> data.SequenceCollection:
        self.seq_len = len(ts)
        return super().sequences(ts)


class RRCF(model.Model):
    @staticmethod
    def model_id(ts: data.TimeSeries, **kwargs) -> str:
        return f"rrcf_{ts.name}_{'_'.join([str(val) for val in kwargs.values()])}"

    def __init__(self, n_trees: int, tree_size: int):
        self.n_trees = n_trees
        self.tree_size = tree_size

    def do_train(self, sequences: data.SequenceCollection, **kwargs) -> "RRCF":
        return self

    def predict(self, sequences: data.SequenceCollection, **kwargs) -> data.TimeSeries:
        X = (
            sequences[0].data
            if len(sequences) == 1
            else np.array([s.data for s in sequences]).squeeze()
        )
        n = X.shape[0]
        if n < self.tree_size:
            self.tree_size = n

        forest = []
        X = np.nan_to_num(X)

        while len(forest) < self.n_trees:
            # Select random subsets of points uniformly from point set
            idxs = np.random.choice(
                n, size=(n // self.tree_size, self.tree_size), replace=False
            )
            # Add sampled trees to forest
            trees = [rrcf.RCTree(X[idx], index_labels=idx) for idx in idxs]
            forest += trees
        # Compute average CoDisp
        avg_codisp = pd.Series(0.0, index=np.arange(n))
        index = np.zeros(n)
        for tree in forest:
            codisp = pd.Series({leaf: tree.codisp(leaf) for leaf in tree.leaves})
            avg_codisp[codisp.index] += codisp
            np.add.at(index, codisp.index.values, 1)
        avg_codisp /= index
        scores = avg_codisp.to_numpy()
        # change nan values to 0
        nans = np.where(np.isnan(scores))[0]
        scores[nans] = np.nanmin(scores)

        # scale scores to time series length
        scores = np.array(
            [np.full(sequences[0].stride, val) for val in scores]
        ).flatten()
        pad_length = sequences[0].ts.length - scores.shape[0]
        scores = np.concatenate([scores, np.ones(pad_length) * scores[-1]])

        return data.TimeSeries.from_numpy(scores.reshape((-1, 1)))
