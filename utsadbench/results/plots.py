# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import pathlib

import matplotlib.pyplot as plt
import mlflow

import utsadbench.core.data as data
from utsadbench.results.classifier import AbstractAnomalyClassifier


class AnomalyDetectionPlot:
    def __init__(self, path: str):
        self.path = path

    def create_plot(
        self,
        ts: data.TimeSeries,
        classifier: AbstractAnomalyClassifier,
        prediction: data.TimeSeries = None,
        dpi: int = 72,
    ) -> plt.Figure:
        fig, ax = plt.subplots(
            nrows=ts.dim + 1, figsize=(15, 6), sharex=True, dpi=dpi, squeeze=False
        )

        # plot time series
        for d in range(ts.dim):
            ax[d, 0].plot(ts.data[:, d], label="Original Timeseries")
            if prediction is not None:
                ax[d, 0].plot(prediction.data[:, d], alpha=0.8, label="Prediction")
            ax[d, 0].legend()
            # ground truth anomalies
            for anomaly in ts.anomalies:
                self._highlight_anomaly(ax[d, 0], anomaly, color="red")

        # plot anomaly score
        ax[-1, 0].plot(
            classifier.scores.data, linewidth=0.5, label="Anomaly Score", color="red"
        )
        # plot threshold
        ax[-1, 0].axhline(y=classifier.threshold, color="hotpink", label="Threshold")
        ax[-1, 0].legend()
        # plot detected anomalies
        for anomaly in classifier.anomalies:
            self._highlight_anomaly(ax[-1, 0], anomaly, color="green")

        fig.suptitle(ts.name)
        fig.tight_layout()

        return fig

    def to_file(
        self,
        ts: data.TimeSeries,
        classifier: AbstractAnomalyClassifier,
        prediction: data.TimeSeries = None,
        dpi: int = 150,
    ):
        fig = self.create_plot(ts, classifier, prediction, dpi)
        fig.savefig(f"{self.path}/{ts.name}_anomalies.png")
        plt.close(fig)

    def to_mlflow(
        self,
        ts: data.TimeSeries,
        classifier: AbstractAnomalyClassifier,
        prediction: data.TimeSeries = None,
        dpi: int = 72,
    ):
        fig = self.create_plot(ts, classifier, prediction, dpi)
        mlflow.log_figure(fig, f"{ts.name}.png")

    def _highlight_anomaly(
        self, ax: plt.Axes, anomaly: data.dataset.Anomaly, color: str
    ):
        ax.axvspan(
            anomaly.start, anomaly.start + anomaly.length, color=color, alpha=0.5, lw=1
        )
