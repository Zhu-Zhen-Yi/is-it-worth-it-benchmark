# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import pathlib
import pickle

import omegaconf


class PickleExporter:
    @staticmethod
    def export(data, name: str, model_id: str, cfg: omegaconf.DictConfig):
        pathlib.Path(cfg.intermediate_dir).mkdir(parents=True, exist_ok=True)
        path = f"{cfg.intermediate_dir}/{model_id}_{name}.pkl"
        with open(path, "wb") as f:
            pickle.dump({name: data}, f)
