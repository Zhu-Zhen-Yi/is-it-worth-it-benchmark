# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import abc
import logging
from typing import List, Optional, Union

import numpy as np
import numpy.typing as npt

import lib.pot.src.pot as pot
import lib.pot.src.spot as spot
import utsadbench.core.data as data
from utsadbench.core.data.dataset import Anomaly

logger = logging.Logger(__name__)


class AbstractAnomalyClassifier(abc.ABC):
    def __init__(self, scores: data.TimeSeries, threshold: float, ts: data.TimeSeries):
        self.scores: data.TimeSeries = scores
        self.threshold: float = threshold
        self.ts = ts
        self.predicted_labels: npt.NDArray[np.bool] = self.scores.data >= threshold
        self.anomalies: List[Anomaly] = self.anomalies_from_labels(
            self.predicted_labels
        )

        self.latency: Optional[float] = None

    @classmethod
    def classify(
        cls, scores: data.TimeSeries, ts: data.TimeSeries, **kwargs
    ) -> "AbstractAnomalyClassifier":
        threshold = scores.max
        try:
            threshold = cls.calculate_threshold(scores, **kwargs)
        except ValueError:
            pass

        return cls(scores, threshold, ts)

    @staticmethod
    @abc.abstractmethod
    def calculate_threshold(scores: data.TimeSeries, **kwargs) -> float:
        pass

    @staticmethod
    def anomalies_from_labels(labels: npt.NDArray[np.bool]) -> List[Anomaly]:
        if np.sum(labels) == 0:
            return []
        aseqs = np.nonzero(labels)[0]
        aseqs = np.append(aseqs, aseqs[-1] + 10)
        anomalies = []
        last_diff = 0
        sequence_count = 0
        current_start = aseqs[0]
        for i in range(1, len(aseqs)):
            if last_diff > 1:
                anomaly = Anomaly(current_start, sequence_count)
                anomalies.append(anomaly)
                current_start = aseqs[i - 1]
                sequence_count = 0

            diff = aseqs[i] - aseqs[i - 1]
            last_diff = diff
            sequence_count += 1
        anomaly = Anomaly(current_start, sequence_count)
        anomalies.append(anomaly)

        return anomalies

    def adjust_labels(self, backwards: bool = False):
        """Adjust length of found true anomalies.

        If a true anomaly has been found, mark the whole segment as found
        based on the ground truth label. This allows comparison of results
        independent of chosen windowsize or stride.

        Source:
        Xu, Haowen, et al.
        "Unsupervised anomaly detection via variational auto-encoder for seasonal kpis
        in web applications." Proceedings of the 2018 world wide web conference. 2018.

        :param backwards: If set to true, also false negatives before
        the found anomaly will be relabelled. If false, those would be ignored
        and just be used for latency calculation

        Code inspired by:
        https://github.com/NetManAIOps/OmniAnomaly/blob/master/omni_anomaly/eval_methods.py

        :return: The adjusted predictions as boolean numpy array and the average latency
        """
        latency = -1
        anomaly_state = False
        anomaly_count = 0
        marked = 0
        predict = self.predicted_labels
        actual = self.ts.labels > 0.1

        for i in range(len(self.scores)):
            if actual[i] and predict[i] and not anomaly_state:
                anomaly_state = True
                anomaly_count += 1
                for j in range(i, 0, -1):
                    if not actual[j]:
                        break
                    elif not predict[j]:
                        latency += 1
                        if backwards:
                            marked += 1
                            predict[j] = True
            elif not actual[i] and anomaly_state:
                anomaly_state = False

            if not predict[i] and anomaly_state:
                marked += 1
                predict[i] = True

        self.predicted_labels = np.array(predict)
        self.anomalies = self.anomalies_from_labels(self.predicted_labels)
        self.latency = latency / (anomaly_count + 1e-4)


class POTAnomalyClassifier(AbstractAnomalyClassifier):
    @staticmethod
    def calculate_threshold(
        scores: data.TimeSeries, init_level=0.98, risk=1e-4
    ) -> float:
        """Calculate thresholds based on the POT method.

        Calculate thresholds based on the Peak over Threshold (POT) method.

        Source:
            Siffer, Alban, et al.
            "Anomaly detection in streams with extreme value theory."
            Proceedings of the 23rd ACM SIGKDD International Conference on
            Knowledge Discovery and Data Mining. 2017.

        Repository: https://github.com/cbhua/model-pot

        Args:
            scores: utsadbench.core.data.TimeSeries
                A TimeSeries containing the anomaly scores
            init_level: init_level: probability associated with the initial threshold
            risk: Detection level

        Returns: float

        """
        threshold, _ = pot.pot(
            np.squeeze(scores.data), init_level=init_level, risk=risk
        )

        return threshold


class SPOTAnomalyClassifier(AbstractAnomalyClassifier):
    @staticmethod
    def calculate_threshold(
        scores: data.TimeSeries, init_level: Union[int, float] = 0.1, risk: float = 1e-4
    ) -> float:
        """Calculate thresholds based on the SPOT method.

        Calculate thresholds based on the Peak over Threshold (POT) method.

        Source:
            Siffer, Alban, et al.
            "Anomaly detection in streams with extreme value theory."
            Proceedings of the 23rd ACM SIGKDD International Conference on
            Knowledge Discovery and Data Mining. 2017.

        Repository: https://github.com/cbhua/model-pot

        Args:
            scores: utsadbench.core.data.TimeSeries
                A TimeSeries containing the anomaly scores
            init_level: int or float
                float: the first `init_level*len(scores)` scores will be used
                        to calculate initial threshold with POT
                int: the first `init_level` scores will be used to calculate
                        initial threshold with POT
            risk: float
                Detection level

        Returns: float

        """
        num_init = (
            int(len(scores) * init_level)
            if isinstance(init_level, float)
            else init_level
        )
        threshold, _ = spot.spot(
            np.squeeze(scores.data), num_init=num_init, risk=risk
        ).values()

        return np.nanmean(threshold)


class MinScoreClassifier(AbstractAnomalyClassifier):
    @staticmethod
    def calculate_threshold(scores: data.TimeSeries, **kwargs) -> float:
        """
        Use the minimal non-zero anomaly score as threshold.
        """
        th = np.min(np.abs(scores.data[np.nonzero(scores.data)]))
        return th.min()
