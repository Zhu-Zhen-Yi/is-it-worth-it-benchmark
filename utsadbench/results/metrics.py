# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import abc
import logging
import math
from typing import List, Union

import mlflow
import numpy as np
import sklearn.metrics

import utsadbench.core.data as data
import utsadbench.core.utils as utils
import utsadbench.results as results
from utsadbench.results.classifier import AbstractAnomalyClassifier

logger = logging.Logger(__name__)


class Metric(abc.ABC):
    def __init__(self, value: Union[float, int], name: str):
        self.value = value
        self.name = name

    @classmethod
    @abc.abstractmethod
    def calculate(
        cls, ts: data.TimeSeries, classifier: AbstractAnomalyClassifier
    ) -> "Metric":
        pass


class AUROC(Metric):

    NAME = "auroc"

    @classmethod
    def calculate(
        cls, ts: data.TimeSeries, classifier: AbstractAnomalyClassifier
    ) -> "AUROC":
        try:
            auroc = sklearn.metrics.roc_auc_score(ts.labels, classifier.scores.data)
        except ValueError:
            logger.warning("Could not calculate AUROC score. Set to -1")
            auroc = -1.0

        return cls(auroc, cls.NAME)


class F1(Metric):

    NAME = "f1"

    @classmethod
    def calculate(cls, ts: data.TimeSeries, classifier: results.classifier) -> "F1":
        try:
            f1 = sklearn.metrics.f1_score(ts.labels, classifier.predicted_labels)
        except sklearn.exceptions.UndefinedMetricWarning:
            logger.warning("Could not calculate F1 score. Set to -1")
            f1 = -1.0

        return cls(f1, cls.NAME)


class UcrScore(Metric):
    NAME = "ucr score"

    @classmethod
    def calculate(
        cls, ts: data.TimeSeries, classifier: AbstractAnomalyClassifier
    ) -> "UcrScore":
        if len(ts.anomalies) == 0:
            return cls(0, cls.NAME)
        begin = ts.anomalies[0].start
        end = ts.anomalies[0].end
        length = ts.anomalies[0].length

        idx = np.argmax(classifier.scores.data)

        score = (
            1
            if min(begin - length, begin - 100) < idx < max(end + length, end + 100)
            else 0
        )

        return cls(score, cls.NAME)


class Latency(Metric):
    NAME = "latency"

    @classmethod
    def calculate(
        cls, ts: data.TimeSeries, classifier: AbstractAnomalyClassifier
    ) -> "Metric":
        if classifier.latency is None:
            return cls(-1, cls.NAME)
        return cls(math.ceil(classifier.latency), cls.NAME)


class TimeSeriesMetrics(utils.NamedCollection[Metric]):
    @classmethod
    def create(cls, items: List[Metric], **kwargs) -> "TimeSeriesMetrics":
        metrics = super().create(items, **kwargs)
        assert isinstance(metrics, TimeSeriesMetrics)

        return metrics

    def to_mlfow(self):
        for metric in self.items:
            mlflow.log_metric(metric.name, metric.value)

    def get_by_name(self, name: str) -> Metric:
        for m in self.items:
            if m.name == name:
                return m
        raise RuntimeError(f"'{name}' is not a valid metric name")
