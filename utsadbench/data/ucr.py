# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

import itertools
import pathlib
from typing import List

import pandas as pd  # type: ignore

import utsadbench.core as core
import utsadbench.core.data.io as io


class UcrTimeSeriesReader(io.TimeSeriesReader):
    @staticmethod
    def from_file(
        file: pathlib.Path, fill_gaps: bool = False, subsample: str = None
    ) -> core.TimeSeries:
        if not file.exists():
            raise ValueError(f"File {file.name} not found in {file.parent}!")
        parts = file.name.split("_")
        parts[6] = parts[6].split(".")[0]

        df = pd.read_csv(file, header=None).squeeze("columns")
        if len(df) <= 1:
            raise RuntimeError(f"File {file.name} not readable!")

        name = f"{parts[0]}_{parts[1]}_{parts[3]}"
        anomaly_start = int(parts[5])
        anomaly_length = int(parts[6]) - anomaly_start + 1

        return core.TimeSeries.from_numpy(
            df.to_numpy().view().reshape((-1, 1)),
            [(anomaly_start, anomaly_length)],
            name=name,
        )


class UcrDataset(core.Dataset):
    def __init__(self, files: List[pathlib.Path]):
        super(UcrDataset, self).__init__([])
        self.files = files

    @classmethod
    def from_path(
        cls,
        path: str,
        start: int = 0,
        n: int = None,
    ):
        path_ = pathlib.Path(path)

        if not path_.is_dir():
            raise ValueError(f"Directory {path} does not exist.")

        slice_to = start + n if n is not None else None
        files = itertools.islice(sorted(path_.glob("*.txt")), start, slice_to)

        return cls(list(files))

    @classmethod
    def from_whitelist(cls, path: str, ids: List[int], start: int = -1, n: int = 0):
        path_ = pathlib.Path(path)

        if not path_.is_dir():
            raise ValueError(f"Directory {path} does not exist.")
        selected_files = []
        skipped = 0
        for i, file in enumerate(sorted(path_.glob("*.txt"))):
            if i not in ids:
                continue
            if start > -1 and (
                skipped < start or skipped + len(selected_files) >= start + n
            ):
                skipped += 1
                continue
            selected_files.append(file)

        return cls(list(selected_files))

    @classmethod
    def from_blacklist(cls, path: str, ids: List[int], start: int = -1, n: int = 0):
        path_ = pathlib.Path(path)

        if not path_.is_dir():
            raise ValueError(f"Directory {path} does not exist.")

        selected_files = []
        skipped = 0
        for i, file in enumerate(sorted(path_.glob("*.txt"))):
            if i in ids:
                continue
            if start > -1 and (
                skipped < start or skipped + len(selected_files) >= start + n
            ):
                skipped += 1
                continue
            selected_files.append(file)

        return cls(list(selected_files))

    @staticmethod
    def _load_file(file: pathlib.Path) -> core.TimeSeries:
        return UcrTimeSeriesReader.from_file(file)

    def __getitem__(self, item):
        return self._load_file(self.files[item])

    def __len__(self):
        return len(self.files)

    def __iter__(self):
        self._index = 0
        return self

    def __next__(self):
        if self._index < len(self.files):
            result = self._load_file(self.files[self._index])
            self._index += 1
            return result
        raise StopIteration
