#!/bin/bash

# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

DATADIR="data"
TARGET="ucraa"

# check if dataset already installed
if [ -d "$DATADIR/$TARGET" ]; then
  printf "UCR Anomaly Archive already installed!\n"
  exit 0
fi

# create dataroot dir if not exsists
mkdir -p $DATADIR
# create tmp dir
mkdir -p tmp && cd tmp

wget https://www.cs.ucr.edu/~eamonn/time_series_data_2018/UCR_TimeSeriesAnomalyDatasets2021.zip
unzip UCR_TimeSeriesAnomalyDatasets2021.zip -d $TARGET

# move to data dir
mv ./ucraa/AnomalyDatasets_2021/UCR_TimeSeriesAnomalyDatasets2021/FilesAreInHere/UCR_Anomaly_FullData ../$DATADIR/$TARGET

# clean up
cd ..
rm -rf tmp

printf "UCR Anomaly Archive successfully installed in $DATADIR/$TARGET\n"