#!/bin/bash

# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdinand Rewicki
#
# SPDX-License-Identifier: Apache-2.0
#
# Usage:
#
# When running on kratos load cmake and eigen via
#   spack load cmake
#   spack load eigen
# #####################################################

mkdir -p ./tmp
wget -P ./tmp https://github.com/cvjena/libmaxdiv/archive/refs/tags/v1.1.zip
unzip ./tmp/v1.1.zip -d ./tmp
mv ./tmp/libmaxdiv-1.1 ./lib/libmaxdiv
touch ./lib/libmaxdiv/__init__.py

cd ./lib/libmaxdiv/maxdiv/libmaxdiv && mkdir bin && cd bin
cmake -D CMAKE_CXX_FLAGS=-I../eigen ..
make

cd ../../../../../

rm -rf ./tmp

