#!/usr/bin/env python

# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR e.V.), Ferdiand Rewicki
#
# SPDX-License-Identifier: Apache-2.0

from distutils import util
from typing import Any

from setuptools import find_packages, setup  # type: ignore[import]

# ------------ THINGS YOU SHOULD CHANGE -----------#

pkg_name = "utsadbench"
author = "Ferdinand Rewicki"
author_email = "ferdinand.rewicki@dlr.de"
description = (
    "Benchmark Environment for the experiments in the Paper "
    "'Is it worth it? Comparing six deep and classical methods"
    " for unsupervised anomaly detection in time series'"
)
license = "MIT License"
install_requires: list[str] = [
    "hydra-core==1.1.*",
    "hydra-joblib-launcher==1.1.*",
    "torch==1.11.*",
    "numpy==1.21.*",
    "pandas==1.4.*",
    "scikit-learn==1.0.*",
    "matplotlib==3.4.3",
    "tqdm==4.62.*",
    "rrcf==0.4.*",
    "scipy==1.7.*",
    "dgl==0.6.*",
    "SciencePlots==1.0.*",
    "pyreadr==0.4.*",
    "seaborn==0.11.*",
    "merlin@git+https://gitlab.com/dlr-dw/py-merlin.git@v1.0.1",
    "mlflow==1.30.*",
    "hydra-optuna-sweeper==1.2.*",
    "plotly==5.11.*",
    "jupyterlab==3.5.*",
]

##############################################
main_ns: dict[str, Any] = {}
ver_path = util.convert_path(f"{pkg_name}/_version.py")

with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)

setup(
    name=pkg_name,
    version=main_ns["__version__"],
    python_requires=">=3.9",
    packages=find_packages(),
    include_package_data=True,
    setup_requires=[],
    install_requires=install_requires,
    extras_require={
        "dev": [
            "pre-commit==2.15.*",
            "mypy==0.910",
        ],
        "test": [
            "pytest==6.2.*",
            "pytest-cov==3.0.*",
            "pytest-mock==3.7.*",
        ],
    },
    author=author,
    author_email=author_email,
    description=description,
    license=license,
)
