# SPDX-FileCopyrightText: 2022 Shreshth Tuli
#
# SPDX-License-Identifier: BSD-3-Clause

# Data folders
output_folder = "processed"
data_folder = "data"
